package ru.bus55.fragments.map;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Field;
import java.util.List;

import ru.bus55.R;
import ru.bus55.activity.MainActivity;
import ru.bus55.fragments.ChildTabFragment;
import ru.bus55.fragments.TabFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapTabFragment extends Fragment implements TabFragment{

    private FragmentManager myFragmentManager;
    private MySupportMapFragment mySupportMapFragment;

    private ChildTabFragment firstChildTabFragment;

    public MapTabFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map_tab, container, false);

        myFragmentManager = getChildFragmentManager();

        FragmentTransaction myFragmentTransaction = myFragmentManager.beginTransaction();
        mySupportMapFragment = new MySupportMapFragment();
        firstChildTabFragment = mySupportMapFragment;

        myFragmentTransaction.replace(R.id.contentMapTab, mySupportMapFragment);
        myFragmentTransaction.commit();

        return view;
    }

    public String getTitle() {
        return MainActivity.getContext().getResources().getString(R.string.fragment_map_tab_title);
    }

    @Override
    public int getIdContent() {
        return R.id.contentMapTab;
    }

    @Override
    public Fragment getCurrentFragment(){
        Fragment currentFragment = null;
        try {
            currentFragment = myFragmentManager.findFragmentById(getIdContent());
        } catch (NullPointerException e){
            currentFragment = null;
        }

        return currentFragment;
    }

    @Override
    public List<Fragment> getListChildFragments(){
        return myFragmentManager.getFragments();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ChildTabFragment getFirstChildTabFragment() {
        return firstChildTabFragment;
    }

}
