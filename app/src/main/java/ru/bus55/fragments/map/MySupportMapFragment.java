package ru.bus55.fragments.map;


import android.content.DialogInterface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.SimpleArrayMap;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import ru.bus55.R;
import ru.bus55.activity.MainActivity;
import ru.bus55.fragments.ChildTabFragment;
import ru.bus55.fragments.TabFragment;
import ru.bus55.fragments.stations.PredictionsStationFragment;
import ru.bus55.fragments.stations.StationsListFragment;
import ru.bus55.models.JsonParser;
import ru.bus55.models.Station;
import ru.bus55.utils.MyUtils;


/**
 * A simple {@link Fragment} subclass.
 */
public class MySupportMapFragment extends Fragment implements OnMapReadyCallback, ChildTabFragment {

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private static float ZOOM_OMSK_COORDINATES = 9.0f;
    private static float ZOOM_MY_LOCATION_COORDINATES = 16.0f;

    private FragmentManager myFragmentManager;
    private MapView mvMapTab;
    private GoogleMap googleMap;

    private ArrayList<Station> alGeolocStations = null;
    private SimpleArrayMap<String, Station> markersToStations = new SimpleArrayMap<>();

    private ImageButton ibMyLocation2;

    private Marker markerMyLocation;

    private String actionBarTitle = "";

    private Location mLastLocation;

    private View view;

    private Bundle onCreateViewSavedInstanceState;

    public MySupportMapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        ((MainActivity) getActivity()).setMySupportMapFragment(this);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //хак чтобы не терять состояние фрагмента состояние фрагмента
        if ( view != null ) {
            if( (ViewGroup)view.getParent() != null )
                ((ViewGroup)view.getParent()).removeView(view);
            return view;
        }

        view = inflater.inflate(R.layout.fragment_my_support_map, container, false);

        myFragmentManager = this.getParentFragment().getChildFragmentManager();

        mvMapTab = (MapView) view.findViewById(R.id.mvMapTab);
        mvMapTab.onCreate(savedInstanceState);
        onCreateViewSavedInstanceState = savedInstanceState;

        ibMyLocation2 = (ImageButton) view.findViewById(R.id.ibMyLocation2);

        ibMyLocation2.setVisibility(View.GONE);
        ibMyLocation2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMyLocation();
            }
        });

        mvMapTab.getMapAsync(this);

        actionBarTitle = getResources().getString(R.string.fragment_map_tab_title);

        return view;
    }

    public void setMyLocation() {


        ((MainActivity)getActivity()).buildGoogleApiClient(MySupportMapFragment.class.getSimpleName());

        if (((MainActivity)getActivity()).getGoogleApiClient() != null) {
            ((MainActivity) getActivity()).getGoogleApiClient().reconnect();
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onCreateOptionsMenu(menu, inflater);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onPrepareOptionsMenu(menu);
            menu.findItem(R.id.action_refresh).setVisible(false);
            menu.findItem(R.id.action_add_bookmarks).setVisible(false);
            menu.findItem(R.id.action_view_map).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(true);
            ((MainActivity) getActivity()).setDisplayShowHome(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mvMapTab.invalidate();
        mvMapTab.onResume();

        //hack for freeze
        MyUtils.openCloseOptionMenuForHackMapViewFreezes(new Handler(), (MainActivity)getActivity());
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(actionBarTitle);

    }

    @Override
    public void onPause() {
        super.onPause();
        mvMapTab.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mvMapTab.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mvMapTab.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        initGoogleMap(googleMap);
    }

    public void initGoogleMap(GoogleMap googleMap) throws SecurityException {
        this.googleMap = googleMap;
        googleMap.setMyLocationEnabled(false);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);

        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                FragmentTransaction myFragmentTransaction = myFragmentManager.beginTransaction();

                PredictionsStationFragment predictionsStationFragment = new PredictionsStationFragment();
                Bundle args = new Bundle();
                args.putSerializable(StationsListFragment.SELECT_STATION, markersToStations.get(marker.getId()));

                predictionsStationFragment.setArguments(args);

                int containerViewId = ((TabFragment) getParentFragment()).getIdContent();
                myFragmentTransaction.replace(containerViewId, predictionsStationFragment);

                myFragmentTransaction.addToBackStack(null);
                myFragmentTransaction.commit();

            }
        });

        ibMyLocation2.setVisibility(View.VISIBLE);
        setOmskCoordinates();

        setMyLocationCoordinates(mLastLocation);
    }


    public void setMyLocationCoordinates(Location location) {

        if (location != null) {

            double lat = location.getLatitude();
            double lng = location.getLongitude();

            LatLng coordinate = new LatLng(lat, lng);

            CameraUpdate center = CameraUpdateFactory.newLatLng(coordinate);
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(ZOOM_MY_LOCATION_COORDINATES);

            BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(MyUtils.getBitmap(MainActivity.getContext(), R.drawable.ic_my_location_marker));

            if (markerMyLocation == null) {
                markerMyLocation = googleMap.addMarker(new MarkerOptions().position(coordinate));
                markerMyLocation.setIcon(icon);
                markerMyLocation.setFlat(false);
                markerMyLocation.setAnchor(0.5f, 0.5f);
                markerMyLocation.setZIndex(0.5f);
            } else {
                markerMyLocation.setPosition(coordinate);
            }

            googleMap.moveCamera(center);
            googleMap.animateCamera(zoom);

            drawStations(coordinate);
        }

    }

    public void setOmskCoordinates() {

        LatLng coordinate = new LatLng(54.988612, 73.324273);
        CameraUpdate center = CameraUpdateFactory.newLatLng(coordinate);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(ZOOM_OMSK_COORDINATES);

        googleMap.moveCamera(center);
        googleMap.animateCamera(zoom);
    }


    class GeolocStationsLoadTask extends AsyncTask<String, Void, Void> {

        private boolean isError = false;

        GeolocStationsLoadTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {

            try {
                JsonParser jsonParser = new JsonParser();
                alGeolocStations = jsonParser.getGeolocStations(params[0], params[1]);
            } catch (Exception e) {
                isError = true;
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (isError) {

                AlertDialog.Builder ad = new AlertDialog.Builder(getContext());

                ad.setTitle("Ошибка");  // заголовок
                ad.setMessage("Ошибка загрузки данных."); // сообщение
                ad.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        return;
                    }
                });

                ad.show();

            } else {

                if (alGeolocStations != null && !alGeolocStations.isEmpty()) {
                    for (Station geolocStation : alGeolocStations) {
                        BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(MyUtils.getBitmap(getContext(), R.drawable.ic_station_marker));
                        Marker marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(geolocStation.getLat()), Double.valueOf(geolocStation.getLon()))).title(geolocStation.getName()));
                        marker.setIcon(icon);
                        marker.setFlat(true);
                        marker.setAnchor(0.5f, 0.5f);
                        marker.setInfoWindowAnchor(0.5f, 0.5f);
                        marker.setZIndex(0.0f);
                        markersToStations.put(marker.getId(), geolocStation);
                    }

                }

            }
        }
    }

    public String getTitle() {
        return MainActivity.getContext().getResources().getString(R.string.fragment_map_tab_title);
    }

    private void drawStations(LatLng coordinate) {
        GeolocStationsLoadTask geolocStationsLoadTask = new GeolocStationsLoadTask();
        geolocStationsLoadTask.execute(String.valueOf(coordinate.latitude), String.valueOf(coordinate.longitude));
    }

    @Override
    public void asyncTaskExecute() {

    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public String getActionBarTitle() {
        return actionBarTitle;
    }

    public Location getLastLocation() {
        return mLastLocation;
    }

    public void setLastLocation(Location location) {
        mLastLocation = location;
    }

    public boolean isInitGoogleMap() {
        return googleMap != null;
    }
}
