package ru.bus55.fragments;

import android.support.v4.app.Fragment;

import java.util.List;

/**
 * Created by IALozhnikov on 13.05.2016.
 */
public interface TabFragment {

    public String getTitle();

    public int getIdContent();

    public List<Fragment> getListChildFragments();

    public Fragment getCurrentFragment();

    public ChildTabFragment getFirstChildTabFragment();
}
