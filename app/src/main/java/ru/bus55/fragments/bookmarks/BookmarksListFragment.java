package ru.bus55.fragments.bookmarks;


import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;

import ru.bus55.R;
import ru.bus55.activity.MainActivity;
import ru.bus55.adapters.BookmarkRoutesListAdapter;
import ru.bus55.adapters.BookmarkStationListAdapter;
import ru.bus55.db.DatabaseHelper;
import ru.bus55.fragments.ChildTabFragment;
import ru.bus55.fragments.TabFragment;
import ru.bus55.fragments.routes.RouteDirectionsFragment;
import ru.bus55.fragments.routes.RoutesListFragment;
import ru.bus55.fragments.stations.PredictionsStationFragment;
import ru.bus55.fragments.stations.StationsListFragment;
import ru.bus55.models.Route;
import ru.bus55.models.Station;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookmarksListFragment extends Fragment implements ChildTabFragment {

    private TabLayout tlBookmarksTab;
    private LinearLayout llBookmarksTab;
    private ListView lvBookmarkRoutes;
    private ListView lvBookmarkStations;
    private ProgressBar pbBookmarksTab;
    private BookmarkRoutesListAdapter bookmarkRoutesListAdapter;
    private BookmarkStationListAdapter bookmarkStationListAdapter;
    private ArrayList<Route> alBookmarkRoutes;
    private ArrayList<Station> alBookmarkStation;
    private FragmentManager myFragmentManager;

    private String actionBarTitle = "";

    private View view;

    public BookmarksListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if ( view != null ) {
            if ( (ViewGroup)view.getParent() != null ) {
                ((ViewGroup) view.getParent()).removeView(view);
            }
            return view;
        }

        view = inflater.inflate(R.layout.fragment_bookmarks_list, container, false);

        myFragmentManager = this.getParentFragment().getChildFragmentManager();
        tlBookmarksTab = (TabLayout) view.findViewById(R.id.tlBookmarksTab);
        llBookmarksTab = (LinearLayout) view.findViewById(R.id.llBookmarksTab);
        lvBookmarkRoutes = (ListView) view.findViewById(R.id.lvBookmarkRoutes);
        lvBookmarkStations = (ListView) view.findViewById(R.id.lvBookmarkStations);
        pbBookmarksTab = (ProgressBar) view.findViewById(R.id.pbBookmarksTab);

        actionBarTitle = getResources().getString(R.string.fragment_bookmarks_tab_title);

        tlBookmarksTab.addTab(tlBookmarksTab.newTab().setText("Маршруты"), 0);
        tlBookmarksTab.addTab(tlBookmarksTab.newTab().setText("Остановки"), 1);
        tlBookmarksTab.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    lvBookmarkRoutes.setVisibility(View.VISIBLE);
                }

                if (tab.getPosition() == 1) {
                    lvBookmarkStations.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    lvBookmarkRoutes.setVisibility(View.GONE);
                }


                if (tab.getPosition() == 1) {
                    lvBookmarkStations.setVisibility(View.GONE);
                }

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        //Сделать селект, чтобы listView показать, если  сделать tlBookmarksTab.getTabAt(0).select(), то событие селекта не срабатывает
        tlBookmarksTab.getTabAt(1).select();
        tlBookmarksTab.getTabAt(0).select();

        lvBookmarkRoutes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Route selectRoute = alBookmarkRoutes.get(position);
                FragmentTransaction myFragmentTransaction = myFragmentManager.beginTransaction();
                RouteDirectionsFragment routesDirectionsFragment = new RouteDirectionsFragment();
                Bundle args = new Bundle();
                args.putSerializable(RoutesListFragment.SELECT_ROUTE, selectRoute);
                routesDirectionsFragment.setArguments(args);

                int containerViewId = ((TabFragment)getParentFragment()).getIdContent();
                myFragmentTransaction.replace(containerViewId, routesDirectionsFragment);

                myFragmentTransaction.addToBackStack(null);
                myFragmentTransaction.commit();
            }
        });

        lvBookmarkStations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Station selectStation = alBookmarkStation.get(position);

                FragmentTransaction myFragmentTransaction = myFragmentManager.beginTransaction();

                PredictionsStationFragment predictionsStationFragment = new PredictionsStationFragment();
                Bundle args = new Bundle();
                args.putSerializable(StationsListFragment.SELECT_STATION, selectStation);
                predictionsStationFragment.setArguments(args);

                int containerViewId = ((TabFragment)getParentFragment()).getIdContent();
                myFragmentTransaction.replace(containerViewId, predictionsStationFragment);

                myFragmentTransaction.addToBackStack(null);
                myFragmentTransaction.commit();
            }
        });

        /*Новый способ*/
        alBookmarkRoutes = MainActivity.getAlBookmarkRoutes();
        alBookmarkStation = MainActivity.getAlStations();


        if (lvBookmarkRoutes.getAdapter() == null) {
            ArrayList<Route> alForAdater = new ArrayList<>();
            boolean b = alForAdater.addAll(alBookmarkRoutes);
            bookmarkRoutesListAdapter = new BookmarkRoutesListAdapter(MainActivity.getContext(), alForAdater);//new StationsByRouteAdapter(MainActivity.getContext(), alAB);
            lvBookmarkRoutes.setAdapter(bookmarkRoutesListAdapter);
        } else {
            bookmarkRoutesListAdapter.getList().clear();
            bookmarkRoutesListAdapter.getList().addAll(alBookmarkRoutes);
            bookmarkRoutesListAdapter.notifyDataSetChanged();
        }

        //если приложение открылось и адаптер не создан
        if (lvBookmarkStations.getAdapter() == null) {
            ArrayList<Station> alForAdater = new ArrayList<>();
            boolean b = alForAdater.addAll(alBookmarkStation);
            bookmarkStationListAdapter = new BookmarkStationListAdapter(MainActivity.getContext(), alForAdater);//new StationsByRouteAdapter(MainActivity.getContext(), alAB);
            lvBookmarkStations.setAdapter(bookmarkStationListAdapter);
        } else {//иначе приложение открыто и адаптер обновляем
            bookmarkStationListAdapter.getList().clear();
            bookmarkStationListAdapter.getList().addAll(alBookmarkStation);
            bookmarkStationListAdapter.notifyDataSetChanged();
        }


        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onCreateOptionsMenu(menu, inflater);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onPrepareOptionsMenu(menu);
            menu.findItem(R.id.action_refresh).setVisible(false);
            menu.findItem(R.id.action_add_bookmarks).setVisible(false);
            menu.findItem(R.id.action_view_map).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(true);
            ((MainActivity) getActivity()).setDisplayShowHome(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Fragment p = getParentFragment();
        if (p.getUserVisibleHint()) {
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(actionBarTitle);
        }
        asyncTaskExecute();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void asyncTaskExecute() {
        BookmarksLoadTask bookmarksLoadTask = new BookmarksLoadTask();
        bookmarksLoadTask.execute();
    }

    @Override
    public void hideKeyboard() {

    }

    class BookmarksLoadTask extends AsyncTask<Void, Void, Void> {

        BookmarksLoadTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbBookmarksTab.setVisibility(View.VISIBLE);
            llBookmarksTab.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            DatabaseHelper databaseHelper = new DatabaseHelper(MainActivity.getContext());
            alBookmarkRoutes = databaseHelper.getRoutes(null);
            alBookmarkStation = databaseHelper.getStation(null);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (lvBookmarkRoutes.getAdapter() == null) {
                ArrayList<Route> alForAdater = new ArrayList<>();
                boolean b = alForAdater.addAll(alBookmarkRoutes);
                bookmarkRoutesListAdapter = new BookmarkRoutesListAdapter(MainActivity.getContext(), alForAdater);//new StationsByRouteAdapter(MainActivity.getContext()/*getActivity().getApplicationContext()*/, alAB);
                lvBookmarkRoutes.setAdapter(bookmarkRoutesListAdapter);
            } else {
                bookmarkRoutesListAdapter.getList().clear();
                bookmarkRoutesListAdapter.getList().addAll(alBookmarkRoutes);
                bookmarkRoutesListAdapter.notifyDataSetChanged();
            }

            //если приложение открылось и адаптер не создан
            if (lvBookmarkStations.getAdapter() == null) {
                ArrayList<Station> alForAdater = new ArrayList<>();
                boolean b = alForAdater.addAll(alBookmarkStation);
                bookmarkStationListAdapter = new BookmarkStationListAdapter(MainActivity.getContext(), alForAdater);//new StationsByRouteAdapter(MainActivity.getContext()/*getActivity().getApplicationContext()*/, alAB);
                lvBookmarkStations.setAdapter(bookmarkStationListAdapter);
            } else {//иначе приложение открыто и адаптер обновляем
                bookmarkStationListAdapter.getList().clear();
                bookmarkStationListAdapter.getList().addAll(alBookmarkStation);
                bookmarkStationListAdapter.notifyDataSetChanged();
            }

            pbBookmarksTab.setVisibility(View.GONE);
            llBookmarksTab.setVisibility(View.VISIBLE);

        }
    }

    public void addRouteToBookmark(Route selectRoute) {
        AddRouteToBookmarkLoadTask addRouteToBookmarkLoadTask = new AddRouteToBookmarkLoadTask();
        addRouteToBookmarkLoadTask.execute(selectRoute);
    }

    public void removeRouteFromBookmark(Route selectRoute) {
        final Route iSelectRoute = selectRoute;
        final MainActivity mainActivity = (MainActivity)MainActivity.getContext();
        AlertDialog.Builder ad = new AlertDialog.Builder(MainActivity.getContext());

        ad.setTitle("Удаление из закладок");  // заголовок
        ad.setMessage("Вы действительно хотите удалить маршрут №" + selectRoute.getName() ); // сообщение
        ad.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                RemoveRouteFromBookmarkLoadTask removeRouteFromBookmarkLoadTask = new RemoveRouteFromBookmarkLoadTask();
                removeRouteFromBookmarkLoadTask.execute(iSelectRoute);
            }
        });
        ad.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                return;
            }
        });
        ad.setCancelable(false);

        ad.show();

    }

    public void addStationToBookmark(Station selectStation) {
        AddStationToBookmarkLoadTask addStationToBookmarkLoadTask = new AddStationToBookmarkLoadTask();
        addStationToBookmarkLoadTask.execute(selectStation);
    }

    public void removeStationFromBookmark(Station selectStation) {
        final Station iSelectStation = selectStation;
        final MainActivity mainActivity = (MainActivity)MainActivity.getContext();
        AlertDialog.Builder ad = new AlertDialog.Builder(MainActivity.getContext());

        ad.setTitle("Удаление из закладок");  // заголовок
        ad.setMessage("Вы действительно хотите удалить остановку " + iSelectStation.getName() ); // сообщение
        ad.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                RemoveStationFromBookmarkLoadTask removeStationFromBookmarkLoadTask = new RemoveStationFromBookmarkLoadTask();
                removeStationFromBookmarkLoadTask.execute(iSelectStation);
            }
        });
        ad.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                return;
            }
        });
        ad.setCancelable(false);


        ad.show();

    }

    class AddRouteToBookmarkLoadTask extends AsyncTask<Route, Void, Void> {

        private Route selectRoute;
        private boolean isAdd = false;

        AddRouteToBookmarkLoadTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Route... params) {
            selectRoute = params[0];
            DatabaseHelper databaseHelper = new DatabaseHelper(MainActivity.getContext());
            ArrayList<Route> alRoutes = databaseHelper.getRoutes(selectRoute);

            if (alRoutes.size() > 0) {
                isAdd = false;
            } else {
                databaseHelper.insertRoute(selectRoute);
                isAdd = true;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (!isAdd) {
                Toast toast = Toast.makeText(MainActivity.getContext().getApplicationContext(),
                        "Маршрут №" + selectRoute.getName() + " уже добавлен в закладки",
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                Toast toast = Toast.makeText(MainActivity.getContext().getApplicationContext(),
                        "Маршрут №" + selectRoute.getName() + " успешно добавлен в закладки",
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }

    class RemoveRouteFromBookmarkLoadTask extends AsyncTask<Route, Void, Void> {
        private Route selectRoute;
        private boolean isRemove = false;

        RemoveRouteFromBookmarkLoadTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Route... params) {
            selectRoute = params[0];

            DatabaseHelper databaseHelper = new DatabaseHelper(MainActivity.getContext());
            databaseHelper.removeRoute(selectRoute);
            isRemove = true;

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            asyncTaskExecute();

            if (isRemove) {
                Toast toast = Toast.makeText(MainActivity.getContext().getApplicationContext(),
                        "Маршрут №" + selectRoute.getName() + " успешно удален из закладок",
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }

    class AddStationToBookmarkLoadTask extends AsyncTask<Station, Void, Void> {
        private Station selectStation;
        private boolean isAdd = false;

        AddStationToBookmarkLoadTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Station... params) {
            selectStation = params[0];

            DatabaseHelper databaseHelper = new DatabaseHelper(MainActivity.getContext());
            ArrayList<Station> alStation = databaseHelper.getStation(selectStation);

            if (alStation.size() > 0) {
                isAdd = false;
            } else {
                databaseHelper.insertStation(selectStation);
                isAdd = true;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (!isAdd) {
                Toast toast = Toast.makeText(MainActivity.getContext().getApplicationContext(),
                        "Остановка " + selectStation.getName() + " уже добавлена в закладки",
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                Toast toast = Toast.makeText(MainActivity.getContext().getApplicationContext(),
                        "Остановка " + selectStation.getName() + " успешно добавлена в закладки",
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }

    class RemoveStationFromBookmarkLoadTask extends AsyncTask<Station, Void, Void> {
        private Station selectStation;
        private boolean isRemove = false;

        RemoveStationFromBookmarkLoadTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Station... params) {
            selectStation = params[0];

            DatabaseHelper databaseHelper = new DatabaseHelper(MainActivity.getContext());
            isRemove = databaseHelper.removeStation(selectStation);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            asyncTaskExecute();

            if (isRemove) {
                Toast toast = Toast.makeText(MainActivity.getContext().getApplicationContext(),
                        "Остановка " + selectStation.getName() + " успешно удалена из закладок",
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                Toast toast = Toast.makeText(MainActivity.getContext().getApplicationContext(),
                        "Остановка " + selectStation.getName() + " по каким-то причинам не удалена из закладок",
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }

    @Override
    public String getActionBarTitle() {
        return actionBarTitle;
    }

}
