package ru.bus55.fragments.stations;


import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import ru.bus55.R;
import ru.bus55.activity.MainActivity;
import ru.bus55.adapters.DirectionsListAdapter;
import ru.bus55.fragments.ChildTabFragment;
import ru.bus55.fragments.TabFragment;
import ru.bus55.models.Direction;
import ru.bus55.models.InfoNextStation;
import ru.bus55.models.JsonParser;
import ru.bus55.models.Station;

/**
 * A simple {@link Fragment} subclass.
 */
public class DirectionsListFragment extends Fragment implements MainActivity.OnBackPressedListener, ChildTabFragment {

    public static String SELECT_STATION_WITH_DIRECTION = "select_station_with_direction";
    public static SavedState savedState;
    private Station selectStation;

    private ProgressBar pbDirectionsList;
    private ListView lvDirections;
    private TextView tvNoDirections;

    private ArrayList<Direction> alDirections = null;
    private ArrayList<String> directionNames;
    private DirectionsListAdapter adapter;

    private FragmentManager myFragmentManager;
    private DirectionsLoadTask dlt = null;

    private String actionBarTitle = "";

    private View view;

    public DirectionsListFragment() {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if ( view != null ) {
            if ( (ViewGroup)view.getParent() != null ) {
                ((ViewGroup) view.getParent()).removeView(view);
            }
            return view;
        }

        view = inflater.inflate(R.layout.fragment_directions_list, container, false);

        myFragmentManager = this.getParentFragment().getChildFragmentManager();

        pbDirectionsList = (ProgressBar) view.findViewById(R.id.pbDirectionsList);
        lvDirections = (ListView) view.findViewById(R.id.lvDirections);
        tvNoDirections = (TextView) view.findViewById(R.id.tvNoDirections);

        selectStation = (Station) getArguments().getSerializable(StationsListFragment.SELECT_STATION);

        actionBarTitle = selectStation.getName();

        //Получаем данные или восстаналиваем, если существуют
        if ((alDirections == null) || (alDirections.isEmpty())) {
            asyncTaskExecute();
        } else {
            adapter = new DirectionsListAdapter(getActivity().getApplicationContext(), alDirections);
            lvDirections.setAdapter(adapter);
            lvDirections.setVisibility(View.VISIBLE);
        }

        lvDirections.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Direction selectDirection = alDirections.get(position);

                FragmentTransaction myFragmentTransaction = myFragmentManager.beginTransaction();

                PredictionsStationFragment predictionsStationFragment = new PredictionsStationFragment();
                Bundle args = new Bundle();

                //Установить Id текущей остановки и сдедующей остановки, а также имя следующей остановки
                selectStation.setInfoNextStation(new InfoNextStation(selectDirection.getIdStation1(), selectDirection.getIdStation2(), selectDirection.getStationName2()));

                args.putSerializable(StationsListFragment.SELECT_STATION, selectStation);
                predictionsStationFragment.setArguments(args);

                int containerViewId = ((TabFragment)getParentFragment()).getIdContent();
                myFragmentTransaction.replace(containerViewId, predictionsStationFragment);

                myFragmentTransaction.addToBackStack(null);
                myFragmentTransaction.commit();

            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onCreateOptionsMenu(menu, inflater);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onPrepareOptionsMenu(menu);
            menu.findItem(R.id.action_refresh).setVisible(false);
            menu.findItem(R.id.action_add_bookmarks).setVisible(false);
            menu.findItem(R.id.action_view_map).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(true);
            ((MainActivity) getActivity()).setDisplayShowHome(true);
        }
    }

    @Override
    public void onBackPressed() {
        myFragmentManager.popBackStack();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getParentFragment().getUserVisibleHint()) {
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(actionBarTitle);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void asyncTaskExecute() {
        DirectionsLoadTask dlt = new DirectionsLoadTask();
        dlt.execute();
    }

    @Override
    public void hideKeyboard() {

    }

    class DirectionsLoadTask extends AsyncTask<Void, Void, Void> {

        private boolean isError = false;

        DirectionsLoadTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbDirectionsList.setVisibility(View.VISIBLE);
            lvDirections.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                JsonParser jsonParser = new JsonParser();
                alDirections = jsonParser.getDirections(selectStation.getId());
            } catch (Exception e) {
                isError = true;
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (isError) {
                pbDirectionsList.setVisibility(View.GONE);
                lvDirections.setVisibility(View.GONE);

                AlertDialog.Builder ad = new AlertDialog.Builder(MainActivity.getContext());

                ad.setTitle("Ошибка");  // заголовок
                ad.setMessage("Ошибка загрузки данных."); // сообщение
                ad.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        onBackPressed();
                    }
                });

                ad.setCancelable(true);
                ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        onBackPressed();
                    }
                });

                ad.show();

            } else {

                if (alDirections.isEmpty()) {
                    tvNoDirections.setVisibility(View.VISIBLE);
                } else {
                    tvNoDirections.setVisibility(View.GONE);

                    adapter = new DirectionsListAdapter(getActivity().getApplicationContext(), alDirections);
                    lvDirections.setAdapter(adapter);
                }



                pbDirectionsList.setVisibility(View.GONE);
                lvDirections.setVisibility(View.VISIBLE);

            }
        }
    }

    @Override
    public String getActionBarTitle() {
        return actionBarTitle;
    }

}
