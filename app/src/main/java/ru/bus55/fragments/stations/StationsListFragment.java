package ru.bus55.fragments.stations;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;

import org.json.JSONException;

import java.util.ArrayList;

import ru.bus55.R;
import ru.bus55.activity.MainActivity;
import ru.bus55.adapters.StationsListAdapter;
import ru.bus55.fragments.ChildTabFragment;
import ru.bus55.fragments.TabFragment;
import ru.bus55.models.JsonParser;
import ru.bus55.models.Station;

/**
 * A simple {@link Fragment} subclass.
 */
public class StationsListFragment extends Fragment implements SearchView.OnQueryTextListener, ChildTabFragment {

    public static String SELECT_STATION = "select_station";

    private SearchView svSearchStations;
    private ImageButton ibDone;
    private ProgressBar pbStationsList;
    private ListView lvResultStations;

    private ArrayList<Station> alStations = null;

    private StationsListAdapter adapter;

    private FragmentManager myFragmentManager;

    private String actionBarTitle = "";

    private View view;

    public StationsListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onResume() {

        super.onResume();

        if (getParentFragment().getUserVisibleHint()) {
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(actionBarTitle);
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if ( view != null ) {
            if ( (ViewGroup)view.getParent() != null ) {
                ((ViewGroup) view.getParent()).removeView(view);
            }
            return view;
        }

        view = inflater.inflate(R.layout.fragment_stations_list, container, false);

        myFragmentManager = this.getParentFragment().getChildFragmentManager();
        svSearchStations = (SearchView) view.findViewById(R.id.svSearchStations);

        svSearchStations.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {

                    ibDone.setVisibility(View.VISIBLE);
                } else {
                    ibDone.setVisibility(View.GONE);
                }
            }
        });

        svSearchStations.setIconifiedByDefault(false);

        ibDone = (ImageButton) view.findViewById(R.id.ibDoneStations);
        ibDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });

        pbStationsList = (ProgressBar) view.findViewById(R.id.pbStationsList);
        lvResultStations = (ListView) view.findViewById(R.id.lvResultStations);

        actionBarTitle = getResources().getString(R.string.fragment_stations_tab_title);

        //Новый способ
        alStations = new ArrayList<>(MainActivity.getAlStations());
        adapter = new StationsListAdapter(getActivity().getApplicationContext(), alStations);
        lvResultStations.setAdapter(adapter);

        lvResultStations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Station selectStation = alStations.get(position);
                FragmentTransaction myFragmentTransaction = myFragmentManager.beginTransaction();
                ((MainActivity)getActivity()).getSupportActionBar().setTitle(selectStation.getName());
                DirectionsListFragment directionsListFragment = new DirectionsListFragment();
                Bundle args = new Bundle();
                args.putSerializable(SELECT_STATION, selectStation);
                directionsListFragment.setArguments(args);

                int containerViewId = ((TabFragment)getParentFragment()).getIdContent();
                myFragmentTransaction.replace(containerViewId, directionsListFragment);

                myFragmentTransaction.addToBackStack(null);
                myFragmentTransaction.commit();

                // закрыть клаву searchview
                hideKeyboard();

            }
        });

        svSearchStations.setOnQueryTextListener(this);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onCreateOptionsMenu(menu, inflater);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onPrepareOptionsMenu(menu);
            menu.findItem(R.id.action_refresh).setVisible(false);
            menu.findItem(R.id.action_add_bookmarks).setVisible(false);
            menu.findItem(R.id.action_view_map).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(true);
            ((MainActivity) getActivity()).setDisplayShowHome(false);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.filter(newText);
        lvResultStations.setSelectionAfterHeaderView();
        return false;
    }

    @Override
    public void asyncTaskExecute() {
        StationsLoadTask slt = new StationsLoadTask();
        slt.execute();
    }

    @Override
    public void hideKeyboard() {
        if (svSearchStations != null) {
            svSearchStations.clearFocus();
        }
    }

    class StationsLoadTask extends AsyncTask<Void, Void, Void> {

        StationsLoadTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbStationsList.setVisibility(View.VISIBLE);
            lvResultStations.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(Void... params) {

            JsonParser jsonParser = new JsonParser();

            try {
                alStations = jsonParser.getStations();
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            adapter = new StationsListAdapter(getActivity().getApplicationContext(), alStations);
            lvResultStations.setAdapter(adapter);

            pbStationsList.setVisibility(View.GONE);
            lvResultStations.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public String getActionBarTitle() {
        return actionBarTitle;
    }

}
