package ru.bus55.fragments.stations;


import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import ru.bus55.activity.MainActivity;
import ru.bus55.R;
import ru.bus55.adapters.PredictionsStationAdapter;
import ru.bus55.fragments.ChildTabFragment;
import ru.bus55.fragments.TabFragment;
import ru.bus55.fragments.bookmarks.BookmarksListFragment;
import ru.bus55.fragments.routes.RouteDirectionsFragment;
import ru.bus55.fragments.routes.RoutesListFragment;
import ru.bus55.models.JsonParser;
import ru.bus55.models.Prediction;
import ru.bus55.models.Route;
import ru.bus55.models.Station;

/**
 * A simple {@link Fragment} subclass.
 */
public class PredictionsStationFragment extends Fragment implements MainActivity.OnBackPressedListener, ChildTabFragment {

    private Station selectStation;

    private TextView tvPredictionTime;
    private TextView tvNoForecast;
    private ProgressBar pbPredictionsStation;
    private ListView lvPredictions;
    private SwipeRefreshLayout srlPredictions;

    private ArrayList<Prediction> alPredictions = null;
    private ArrayList<String> predictionNames;

    private PredictionsStationAdapter adapter;

    private FragmentManager myFragmentManager;

    private String actionBarTitle = "";

    public PredictionsStationFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_predictions_station, container, false);

        tvPredictionTime = (TextView) view.findViewById(R.id.tvPredictionTime);
        tvNoForecast = (TextView) view.findViewById(R.id.tvNoForecasts);
        pbPredictionsStation = (ProgressBar) view.findViewById(R.id.pbPredictionsStation);
        lvPredictions = (ListView) view.findViewById(R.id.lvPredictions);
        srlPredictions = (SwipeRefreshLayout) view.findViewById(R.id.srlPredictions);

        srlPredictions.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                asyncTaskExecute();
            }
        });

        Bundle bundle = getArguments();
        selectStation = (Station) bundle.getSerializable(StationsListFragment.SELECT_STATION);

        actionBarTitle = selectStation.getName();

        lvPredictions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Prediction selectPrediction = alPredictions.get(position);
                Route selectRoute = new Route();
                selectRoute.setId(selectPrediction.getId());
                selectRoute.setName(selectPrediction.getName());
                selectRoute.setType(selectPrediction.getType());
                selectRoute.setFirst(selectPrediction.getFirst());
                selectRoute.setLast(selectPrediction.getLast());
                selectRoute.setFirstId(selectPrediction.getFirstId());
                selectRoute.setLastId(selectPrediction.getLastId());

                FragmentTransaction myFragmentTransaction = myFragmentManager.beginTransaction();

                RouteDirectionsFragment routesDirectionsFragment = new RouteDirectionsFragment();
                Bundle args = new Bundle();
                args.putSerializable(RoutesListFragment.SELECT_ROUTE, selectRoute);
                routesDirectionsFragment.setArguments(args);

                int containerViewId = ((TabFragment)getParentFragment()).getIdContent();
                myFragmentTransaction.replace(containerViewId, routesDirectionsFragment);

                myFragmentTransaction.addToBackStack(null);
                myFragmentTransaction.commit();

            }
        });

        lvPredictions.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                //включить srlPredictions, когда первая строка в списке
                srlPredictions.setEnabled(firstVisibleItem == 0);
            }
        });

        asyncTaskExecute();

        myFragmentManager = this.getParentFragment().getChildFragmentManager();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getParentFragment().getUserVisibleHint()) {
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(actionBarTitle);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onCreateOptionsMenu(menu, inflater);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onPrepareOptionsMenu(menu);
            menu.findItem(R.id.action_refresh).setVisible(true);
            menu.findItem(R.id.action_add_bookmarks).setVisible(true);
            menu.findItem(R.id.action_view_map).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(true);
            ((MainActivity) getActivity()).setDisplayShowHome(true);
        }
    }

    public void addStationToBookmark() {
        BookmarksListFragment bookmarksListFragment = ((BookmarksListFragment)((MainActivity)getActivity()).findFragment(BookmarksListFragment.class));
        bookmarksListFragment.addStationToBookmark(selectStation);
    }

    @Override
    public void onBackPressed() {
        myFragmentManager.popBackStack();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void asyncTaskExecute() {
        PredictionsLoadTask plt = new PredictionsLoadTask();
        plt.execute();
    }

    @Override
    public void hideKeyboard() {

    }

    class PredictionsLoadTask extends AsyncTask<Void, Void, Void> {

        private boolean isError = false;

        PredictionsLoadTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!srlPredictions.isRefreshing()) {
                pbPredictionsStation.setVisibility(View.VISIBLE);
                lvPredictions.setVisibility(View.GONE);
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                JsonParser jsonParser = new JsonParser();
                //пиздец с остановками
                if (selectStation.getInfoNextStation() == null) {
                    alPredictions = jsonParser.getPredictions(selectStation.getId());
                } else {
                    alPredictions = jsonParser.getPredictions(selectStation.getInfoNextStation().getIdStation1());
                }
            } catch (Exception e) {
                isError = true;
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (isError) {
                pbPredictionsStation.setVisibility(View.GONE);
                lvPredictions.setVisibility(View.GONE);

                AlertDialog.Builder ad = new AlertDialog.Builder(MainActivity.getContext());

                ad.setTitle("Ошибка");  // заголовок
                ad.setMessage("Ошибка загрузки данных."); // сообщение
                ad.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        onBackPressed();
                    }
                });

                ad.setCancelable(true);
                ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        onBackPressed();
                    }
                });

                ad.show();

            } else {

                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                String formattedDate = df.format(c.getTime());
                tvPredictionTime.setText("Обновлено " + formattedDate);

                if (alPredictions.isEmpty()) {
                    tvNoForecast.setVisibility(View.VISIBLE);
                    lvPredictions.setVisibility(View.GONE);
                } else {
                    tvNoForecast.setVisibility(View.GONE);
                    lvPredictions.setVisibility(View.VISIBLE);

                    adapter = new PredictionsStationAdapter(getActivity().getApplicationContext(), alPredictions);
                    lvPredictions.setAdapter(adapter);
                }

                if (!srlPredictions.isRefreshing()){
                    pbPredictionsStation.setVisibility(View.GONE);
                    lvPredictions.setVisibility(View.VISIBLE);
                } else {
                    srlPredictions.setRefreshing(false);
                }
            }
        }
    }

    @Override
    public String getActionBarTitle() {
        return actionBarTitle;
    }

}
