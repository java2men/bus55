package ru.bus55.fragments.stations;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Field;
import java.util.List;

import ru.bus55.activity.MainActivity;
import ru.bus55.R;
import ru.bus55.fragments.ChildTabFragment;
import ru.bus55.fragments.TabFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class StationsTabFragment extends Fragment implements TabFragment {

    private FragmentManager myFragmentManager;

    private ChildTabFragment firstChildTabFragment;

    public StationsTabFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stations_tab, container, false);
        myFragmentManager = getChildFragmentManager();
        FragmentTransaction myFragmentTransaction = myFragmentManager.beginTransaction();
        StationsListFragment stationsListFragment = new StationsListFragment();
        firstChildTabFragment = stationsListFragment;
        myFragmentTransaction.replace(R.id.contentStationsTab, stationsListFragment);
        myFragmentTransaction.commit();

        return view;
    }

    public String getTitle() {
        return MainActivity.getContext().getResources().getString(R.string.fragment_stations_tab_title);
    }

    @Override
    public int getIdContent() {
        return R.id.contentStationsTab;
    }

    @Override
    public Fragment getCurrentFragment(){
        Fragment currentFragment = null;
        try {
            currentFragment = myFragmentManager.findFragmentById(getIdContent());
        } catch (NullPointerException e){
            currentFragment = null;
        }

        return currentFragment;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public ChildTabFragment getFirstChildTabFragment() {
        return firstChildTabFragment;
    }

    @Override
    public List<Fragment> getListChildFragments(){
        return myFragmentManager.getFragments();
    }

}
