package ru.bus55.fragments.settings;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.bus55.R;
import ru.bus55.activity.MainActivity;
import ru.bus55.fragments.ChildTabFragment;
import ru.bus55.fragments.TabFragment;
import ru.bus55.utils.MyUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragmentCompat implements MainActivity.OnBackPressedListener, ChildTabFragment {
    public static String PREF_KEY_LIST_TAB = "pref_key_list_tab";
    public static String PREF_KEY_ROUTES_VIEW = "pref_key_routes_view";

    private FragmentManager myFragmentManager;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ListPreference lpListTab = (ListPreference) getPreferenceManager().findPreference(PREF_KEY_LIST_TAB);
        ListPreference lpRoutesView = (ListPreference) getPreferenceManager().findPreference(PREF_KEY_ROUTES_VIEW);
        Preference pEstimate =  getPreferenceManager().findPreference("pref_key_estimate");
        Preference pFindError =  getPreferenceManager().findPreference("pref_key_find_error");
        Preference pAbout =  getPreferenceManager().findPreference("pref_key_about");

        //обновляем описание - устанавливаем выбранное значение (показывалось в меню)
        lpListTab.setSummary(lpListTab.getEntry());
        lpRoutesView.setSummary(lpRoutesView.getEntry());

        ((MainActivity)getActivity()).getSupportActionBar().setTitle(MainActivity.getContext().getResources().getString(R.string.fragment_setting_title));
        ((MainActivity)getActivity()).setVisibleTabLayout(View.GONE);

        myFragmentManager = this.getParentFragment().getChildFragmentManager();

        lpListTab.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {

                ListPreference lpInner = (ListPreference) preference;
                int index = lpInner.findIndexOfValue(o.toString());
                CharSequence[] entries = lpInner.getEntries();

                if(index >= 0) {
                    lpInner.setSummary(entries[index]);
                }

                return true;
            }
        });

        lpRoutesView.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {

                ListPreference lpInner = (ListPreference) preference;
                int index = lpInner.findIndexOfValue(o.toString());
                CharSequence[] entries = lpInner.getEntries();

                if(index >= 0) {
                    lpInner.setSummary(entries[index]);
                }


                return true;
            }
        });

        pEstimate.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                MyUtils.openAppRating(getContext());
                return true;
            }
        });

        pFindError.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                MyUtils.sendEmail(getContext());
                return true;
            }
        });

        pAbout.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                FragmentTransaction myFragmentTransaction = myFragmentManager.beginTransaction();
                AboutFragment aboutFragment = new AboutFragment();

                int containerViewId = ((TabFragment)getParentFragment()).getIdContent();
                myFragmentTransaction.replace(containerViewId, aboutFragment);

                myFragmentTransaction.addToBackStack(null);
                myFragmentTransaction.commit();

                return true;
            }
        });

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.action_refresh).setVisible(false);
        menu.findItem(R.id.action_add_bookmarks).setVisible(false);
        menu.findItem(R.id.action_view_map).setVisible(false);
        menu.findItem(R.id.action_settings).setVisible(false);
        ((MainActivity)getActivity()).setDisplayShowHome(true);
    }

    @Override
    public void onBackPressed() {
        myFragmentManager.popBackStack();
        ((MainActivity)getActivity()).setVisibleTabLayout(View.VISIBLE);
    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void asyncTaskExecute() {

    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public String getActionBarTitle() {
        return ((MainActivity)getActivity()).getSupportActionBar().getTitle().toString();
    }

}
