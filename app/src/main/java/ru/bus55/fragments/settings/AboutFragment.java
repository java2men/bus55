package ru.bus55.fragments.settings;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.bus55.activity.MainActivity;
import ru.bus55.R;
import ru.bus55.fragments.ChildTabFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment implements MainActivity.OnBackPressedListener, ChildTabFragment {

    private FragmentManager myFragmentManager;

    public AboutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        myFragmentManager = this.getParentFragment().getChildFragmentManager();

        ((MainActivity)getActivity()).getSupportActionBar().setTitle("О программе");

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.action_refresh).setVisible(false);
        menu.findItem(R.id.action_add_bookmarks).setVisible(false);
        menu.findItem(R.id.action_view_map).setVisible(false);
        menu.findItem(R.id.action_settings).setVisible(false);
        ((MainActivity)getActivity()).setDisplayShowHome(true);

    }

    @Override
    public void onBackPressed() {
        myFragmentManager.popBackStack();
    }

    @Override
    public void asyncTaskExecute() {

    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public String getActionBarTitle() {
        return ((MainActivity)getActivity()).getSupportActionBar().getTitle().toString();
    }

}
