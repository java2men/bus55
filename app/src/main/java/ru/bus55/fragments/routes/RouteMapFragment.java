package ru.bus55.fragments.routes;


import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.SimpleArrayMap;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import ru.bus55.R;
import ru.bus55.activity.MainActivity;
import ru.bus55.fragments.ChildTabFragment;
import ru.bus55.fragments.TabFragment;
import ru.bus55.fragments.stations.PredictionsStationFragment;
import ru.bus55.fragments.stations.StationsListFragment;
import ru.bus55.models.Coordinates;
import ru.bus55.models.JsonParser;
import ru.bus55.models.Route;
import ru.bus55.models.Station;
import ru.bus55.models.Vehicle;
import ru.bus55.utils.MyUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class RouteMapFragment extends Fragment implements MainActivity.OnBackPressedListener, OnMapReadyCallback/*, LocationListener*/, ChildTabFragment {

    private static float ZOOM_MY_LOCATION_COORDINATES = 16.0f;
    private static float ZOOM_CENTER_LOCATION_COORDINATES = 11.0f;
    private static int INTERVAL_VEHICLES_SHIFT = 13 * 1000;
    private static float ZOOM_OMSK_COORDINATES = ZOOM_CENTER_LOCATION_COORDINATES;

    private SupportMapFragment smfRouteMap;
    private MapView mvRouteMap;
    private GoogleMap googleMap;
    private FragmentManager myFragmentManager;
    private Route selectRoute;
    private ArrayList<Coordinates> alCoordinates = new ArrayList<>();
    private ArrayList<Station> alStations = new ArrayList<>();
    private SimpleArrayMap<String, Station> markersToStations = new SimpleArrayMap<>();

    private ArrayList<Vehicle> alVehicles = new ArrayList<>();
    private ArrayList<Vehicle> alVehiclesShift = new ArrayList<>();
    private ArrayList<Marker> alVehiclesMarkerBack = new ArrayList<>();
    private ArrayList<Marker> alVehiclesMarkerFront = new ArrayList<>();

    private ImageButton ibMyLocation1;
    private Marker markerMyLocation;

    private Timer timer;
    private TimerTask timerTask;
    //если произошло движение транспорта
    private boolean isVehiclesShift = false;
    //Чтобы отлавливать ошибки и закрывать таймеры
    private static boolean isError = false;

    private boolean isSetMyLocation = false;

    private GoogleApiClient googleApiClient;
    private boolean isRequestLocationUpdates = false;

    private String actionBarTitle = "";

    private Location mLastLocation;

    private View view;

    public RouteMapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setHasOptionsMenu(true);

        //если ссылка на фрагмент в MainActivity отсутствует, то установить ее
        ((MainActivity) getActivity()).setRouteMapFragment(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle s = savedInstanceState;


        //хак чтобы не терять состояние фрагмента состояние фрагмента
        if ( view != null ) {

            if ( (ViewGroup)view.getParent() != null ) {
                ((ViewGroup) view.getParent()).removeView(view);
            }

            return view;

        }


        view = inflater.inflate(R.layout.fragment_route_map, container, false);

        myFragmentManager = this.getParentFragment().getChildFragmentManager();

        selectRoute = (Route) getArguments().get(RoutesListFragment.SELECT_ROUTE);

        StringBuilder sbType = new StringBuilder(selectRoute.getName());

        if (selectRoute.getType().equals(JsonParser.TYPE_BUS)) {
            sbType.append(" автобус");
        }

        if (selectRoute.getType().equals(JsonParser.TYPE_TRAM)) {
            sbType.append(" трамвай");
        }

        if (selectRoute.getType().equals(JsonParser.TYPE_TROLLEYBUS)) {
            sbType.append(" троллейбус");
        }

        if (selectRoute.getType().equals(JsonParser.TYPE_TAXI)) {
            sbType.append(" маршрутное такси");
        }

        actionBarTitle = sbType.toString();

        mvRouteMap = (MapView) view.findViewById(R.id.mvRouteMap);
        mvRouteMap.onCreate(savedInstanceState);

        ibMyLocation1 = (ImageButton) view.findViewById(R.id.ibMyLocation1);

        ibMyLocation1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawMyLocation(true);
            }
        });

        ibMyLocation1.setVisibility(View.GONE);

        mvRouteMap.getMapAsync(this);

        return view;
    }


    public void drawMyLocation(boolean isSet) {

        this.isSetMyLocation = isSet;

        ((MainActivity)getActivity()).buildGoogleApiClient(RouteMapFragment.class.getSimpleName());
        if (((MainActivity)getActivity()).getGoogleApiClient() != null) {
            ((MainActivity) getActivity()).getGoogleApiClient().reconnect();
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onCreateOptionsMenu(menu, inflater);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onPrepareOptionsMenu(menu);
            menu.findItem(R.id.action_refresh).setVisible(false);
            menu.findItem(R.id.action_add_bookmarks).setVisible(false);
            menu.findItem(R.id.action_view_map).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(true);
            ((MainActivity) getActivity()).setDisplayShowHome(true);
        }
    }

    @Override
    public void onResume() {

        super.onResume();
        mvRouteMap.invalidate();
        mvRouteMap.onResume();
        drawVehicles();
        //хак для избежания фриза
        MyUtils.openCloseOptionMenuForHackMapViewFreezes(new Handler(), (MainActivity)getActivity());
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(actionBarTitle);

        //if (getParentFragment().getUserVisibleHint()) {
        if (false) {



            if (((MainActivity) getActivity()).getMySupportMapFragment() != null) {
                if (((MainActivity) getActivity()).getMySupportMapFragment().isResumed()) {
                    ((MainActivity) getActivity()).getMySupportMapFragment().onPause();
                }
            }

            //super.onResume();

            //проверить установленно ли Google Play Service
            //if (MyUtils.checkPlayServices((MainActivity)getActivity())) {
                /*если googleMap еще является null, то это может свидетельствовать о
                * том, что Google Play Service установленны, но в mvMapTab еще висит информация об обновлении,
                * поэтому пересоздадим mvMapTab, чтобы отобразить карту*/
                /*if (googleMap == null) {
                    mvRouteMap.onCreate(null);
                }*/

            //}

            mvRouteMap.invalidate();
            mvRouteMap.onResume();

            //mvRouteMap.postInvalidate();

            //drawRoadAndStation();
            drawVehicles();

            //хак для избежания фриза
            MyUtils.openCloseOptionMenuForHackMapViewFreezes(new Handler(), (MainActivity)getActivity());

        /*Это конечно пиздец товарищи, но это хак века, сука, когда возвращаемся назад на карту,
        то она начинает тормозить, после того как покажем и скроем меню, тормоз карт пропадает*/
            //((MainActivity) getActivity()).openOptionsMenu();
            //((MainActivity) getActivity()).closeOptionsMenu();

        /*
        final Handler myHandler = new Handler();
        Thread myThread = new Thread(new Runnable() {
            @Override
            public void run() {

                myHandler.post(new Runnable() {  // используя Handler, привязанный к UI-Thread
                    @Override
                    public void run() {
                        ((MainActivity) getActivity()).openOptionsMenu();
                        ((MainActivity) getActivity()).closeOptionsMenu();
                    }
                });
            }

        });

        myThread.start();
        */

            //if (getParentFragment().getUserVisibleHint()) {
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(actionBarTitle);


            //}
            //((MainActivity) getActivity()).getSupportActionBar().openOptionsMenu();

            /*
            if (getUserVisibleHint()) {

            }
            */
        } else {
            //super.onPause();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopDrawVehicles();
        //resetMarker();

        mvRouteMap.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mvRouteMap.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mvRouteMap.onLowMemory();
        stopDrawVehicles();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        stopDrawVehicles();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        initGoogleMap(googleMap);

    }

    public void initGoogleMap(GoogleMap googleMap) throws SecurityException {
        //MapsInitializer.initialize(getContext());

        this.googleMap = googleMap;

        setOmskCoordinates();

        googleMap.setMyLocationEnabled(false);
        googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                return false;
            }
        });

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);

        ibMyLocation1.setVisibility(View.VISIBLE);

        //drawMyLocationForRequestPermissions(false);

        //тут
        drawRoadAndStation();
        //Toast.makeText(getContext(), "initGoogleMap", Toast.LENGTH_LONG).show();

        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                //если это остановка, то запустим фрагмент прогноза
                if (markersToStations.containsKey(marker.getId())) {
                    FragmentTransaction myFragmentTransaction = myFragmentManager.beginTransaction();

                    PredictionsStationFragment predictionsStationFragment = new PredictionsStationFragment();
                    Bundle args = new Bundle();
                    args.putSerializable(StationsListFragment.SELECT_STATION, markersToStations.get(marker.getId()));
                    predictionsStationFragment.setArguments(args);


                    int containerViewId = ((TabFragment) getParentFragment()).getIdContent();
                    myFragmentTransaction.replace(containerViewId, predictionsStationFragment);
                    myFragmentTransaction.addToBackStack(null);
                    myFragmentTransaction.commit();

                    //replaceFragment(predictionsStationFragment);
                }
            }
        });

        drawVehicles();
    }

    public void setOmskCoordinates() {

        LatLng coordinate = new LatLng(54.988612, 73.324273);
        CameraUpdate center = CameraUpdateFactory.newLatLng(coordinate);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(ZOOM_OMSK_COORDINATES);

        googleMap.moveCamera(center);
        googleMap.animateCamera(zoom);
    }

    public void drawMyLocationCoordinates(Location location, boolean isSet) {

        double lat =  location.getLatitude();
        double lng = location.getLongitude();

        LatLng coordinate = new LatLng(lat, lng);

        if (markerMyLocation == null) {
            BitmapDescriptor iconMyLocation = BitmapDescriptorFactory.fromBitmap(MyUtils.getBitmap(MainActivity.getContext(),R.drawable.ic_my_location_marker));

            markerMyLocation = googleMap.addMarker(new MarkerOptions().position(coordinate));
            markerMyLocation.setIcon(iconMyLocation);
            markerMyLocation.setFlat(false);
            markerMyLocation.setAnchor(0.5f, 0.5f);
            markerMyLocation.setZIndex(1.0f);
            markerMyLocation.setVisible(true);
        } else {
            markerMyLocation.setPosition(coordinate);
        }

        if (isSet) {
            CameraUpdate center = CameraUpdateFactory.newLatLng(coordinate);
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(ZOOM_MY_LOCATION_COORDINATES);

            googleMap.moveCamera(center);
            googleMap.animateCamera(zoom);

            isSetMyLocation = false;
        }


    }

    public void setCenterCoordinates(LatLng coordinate) {

        CameraUpdate center = CameraUpdateFactory.newLatLng(coordinate);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(ZOOM_CENTER_LOCATION_COORDINATES);

        googleMap.moveCamera(center);
        googleMap.animateCamera(zoom);
    }

    @Override
    public void onBackPressed() {
        myFragmentManager.popBackStack();
    }

    @Override
    public void asyncTaskExecute() {
        drawRoadAndStation();
    }

    @Override
    public void hideKeyboard() {

    }

    class RoutecolGeoLoadTask extends AsyncTask<Void, Void, Void> {

        RoutecolGeoLoadTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                JsonParser jsonParser = new JsonParser();

                Object[][] result = jsonParser.getRoutecolGeo(selectRoute.getId());
                alCoordinates = (ArrayList<Coordinates>) result[0][0];
                alStations = (ArrayList<Station>) result[0][1];
            } catch (Exception e) {
                isError = true;
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (isError) {

                AlertDialog.Builder ad = new AlertDialog.Builder(MainActivity.getContext());

                ad.setTitle("Ошибка");  // заголовок
                ad.setMessage("Ошибка загрузки данных."); // сообщение
                ad.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        return;
                    }
                });

                ad.show();

            } else {
                int lastIndex = alCoordinates.size()/2;
                int middleIndex = lastIndex/2;

                LatLng center = new LatLng(Double.valueOf(alCoordinates.get(middleIndex).getLat()),Double.valueOf(alCoordinates.get(middleIndex).getLon()));
                setCenterCoordinates(center);

                PolylineOptions polylineOptions = new PolylineOptions();
                for (Coordinates coordinates : alCoordinates) {
                    polylineOptions.add(new LatLng(Double.valueOf(coordinates.getLat()), Double.valueOf(coordinates.getLon())));
                }
                googleMap.addPolyline(polylineOptions.width(2).color(Color.BLUE));

                for (Station station : alStations) {
                    BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(MyUtils.getBitmap(MainActivity.getContext(), R.drawable.ic_station_marker));
                    Marker marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(station.getLat()), Double.valueOf(station.getLon()))).title(station.getName()));
                    marker.setIcon(icon);
                    marker.setFlat(true);
                    marker.setAnchor(0.5f, 0.5f);
                    marker.setInfoWindowAnchor(0.5f, 0.5f);
                    marker.setZIndex(0.0f);
                    markersToStations.put(marker.getId(), station);
                }

                //нарисовать свое местоположение
                drawMyLocation(false);

            }

        }
    }

    class VehiclesCoordinatesLoadTask extends AsyncTask<Void, Void, Void> {

        VehiclesCoordinatesLoadTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                JsonParser jsonParser = new JsonParser();
                if (isVehiclesShift) {
                    alVehiclesShift = jsonParser.getVehicles(selectRoute.getId());
                } else {
                    alVehicles = jsonParser.getVehicles(selectRoute.getId());
                }

            } catch (Exception e) {
                isError = true;
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (isError) {
                stopDrawVehicles();
            } else {

                if (isVehiclesShift) {

                    for (int i = 0; i < alVehicles.size(); i++) {
                        Vehicle vehicle = alVehicles.get(i);
                        for (Vehicle vehicleShift : alVehiclesShift) {
                            if (vehicle.getId().equals(vehicleShift.getId())) {
                                if ((!vehicle.getLat().equals(vehicleShift.getLat())) ||  (!vehicle.getLon().equals(vehicleShift.getLon()))) {
                                    vehicle = vehicleShift;

                                    Marker markerBack = alVehiclesMarkerBack.get(i);
                                    markerBack.setPosition(new LatLng(Double.valueOf(vehicle.getLat()), Double.valueOf(vehicle.getLon())));
                                    //markerBack.setTitle(vehicle.getInfo());
                                    markerBack.setRotation(Float.valueOf(vehicle.getCourse()));

                                    Marker markerFront = alVehiclesMarkerFront.get(i);
                                    markerFront.setPosition(new LatLng(Double.valueOf(vehicle.getLat()), Double.valueOf(vehicle.getLon())));
                                    markerFront.setTitle(vehicle.getInfo());

                                    break;
                                }
                            }
                        }

                    }

                } else {

                    float zindexBack = 0.00001f;
                    float zindexFront = 0.0001f;
                    float zindexLast = 0.5f;
                    //добавить маркеры с новыми координатами
                    for (Vehicle vehicle : alVehicles) {

                        BitmapDescriptor iconFront = BitmapDescriptorFactory.fromBitmap(MyUtils.getBitmap(MainActivity.getContext(), R.drawable.ic_vehicle_marker_front));
                        Marker markerFront = googleMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(vehicle.getLat()), Double.valueOf(vehicle.getLon()))).title(vehicle.getInfo()));
                        markerFront.setIcon(iconFront);
                        markerFront.setFlat(false);
                        markerFront.setAnchor(0.5f, 0.5f);
                        markerFront.setInfoWindowAnchor(0.5f, 0.5f);
                        markerFront.setZIndex(zindexLast + zindexFront);
                        alVehiclesMarkerFront.add(markerFront);

                        BitmapDescriptor iconBack = BitmapDescriptorFactory.fromBitmap(MyUtils.getBitmap(MainActivity.getContext(), R.drawable.ic_vehicle_marker_back));
                        Marker markerBack = googleMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(vehicle.getLat()), Double.valueOf(vehicle.getLon())))/*.title(vehicle.getInfo())*/);
                        markerBack.setIcon(iconBack);
                        markerBack.setFlat(true);
                        markerBack.setAnchor(0.5f, 0.57f);
                        markerBack.setRotation(Float.valueOf(vehicle.getCourse()));
                        markerBack.setZIndex(markerFront.getZIndex() - zindexBack);
                        alVehiclesMarkerBack.add(markerBack);

                        zindexLast = markerFront.getZIndex();
                    }

                    synchronized (this) {
                        isVehiclesShift = true;
                    }

                }

            }

        }
    }

    public void drawRoadAndStation(){
        isError = false;
        RoutecolGeoLoadTask routecolGeoLoadTask = new RoutecolGeoLoadTask();
        routecolGeoLoadTask.execute();
    }

    public boolean drawVehicles() {

        if (googleMap == null) {
            return false;
        }

        final Handler handler = new Handler();
        if (timer == null) {
            timer = new Timer();
            timerTask = new TimerTask() {
                @Override
                public void run() {

                    try {
                        VehiclesCoordinatesLoadTask vehiclesCoordinatesLoadTask = new VehiclesCoordinatesLoadTask();
                        vehiclesCoordinatesLoadTask.execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                        isVehiclesShift = false;
                    }

                }
            };
            timer.schedule(timerTask, 0, INTERVAL_VEHICLES_SHIFT); //execute in every 10000 ms
        }

        return true;
    }

    public void stopDrawVehicles() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
            if (timerTask != null) {
                if (timerTask.cancel()){
                    if (isError) {
                        AlertDialog.Builder ad = new AlertDialog.Builder(MainActivity.getContext());

                        ad.setTitle("Ошибка");  // заголовок
                        ad.setMessage("Ошибка загрузки данных."); // сообщение
                        ad.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int arg1) {
                                return;
                            }
                        });

                        ad.show();
                    }
                    timerTask = null;
                }


            }
        }

    }

    @Override
    public String getActionBarTitle() {
        return actionBarTitle;
    }

    public void showDialogRationale() {
        AlertDialog.Builder ad = new AlertDialog.Builder(MainActivity.getContext());

        ad.setTitle("Невозможно установить ваше текущее местоположение");  // заголовок
        ad.setMessage("Вы запретили разрешение на определение вашего местоположения. Для того чтобы разрешить, зайдите в настройки разрешений."); // сообщение
        ad.setPositiveButton("Понятно", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                return;
            }
        });

        ad.show();
    }

    public Location getLastLocation() {
        return mLastLocation;
    }

    public void setLastLocation(Location location) {
        mLastLocation = location;
    }

    public boolean isSetMyLocation() {
        return isSetMyLocation;
    }

    public boolean isInitGoogleMap() {
        return googleMap != null;
    }


    public Bitmap bitmapSizeByScall( Bitmap bitmapIn, float scall_zero_to_one_f) {

        Bitmap bitmapOut = Bitmap.createScaledBitmap(bitmapIn,
                Math.round(bitmapIn.getWidth() * scall_zero_to_one_f),
                Math.round(bitmapIn.getHeight() * scall_zero_to_one_f), false);

        return bitmapOut;
    }

}
