package ru.bus55.fragments.routes;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;

import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;

import ru.bus55.R;
import ru.bus55.activity.MainActivity;
import ru.bus55.adapters.RoutesListAdapter;
import ru.bus55.fragments.ChildTabFragment;
import ru.bus55.fragments.TabFragment;
import ru.bus55.models.JsonParser;
import ru.bus55.models.Route;

/**
 * A simple {@link Fragment} subclass.
 */
public class RoutesListFragment extends Fragment implements /*MainActivity.OnBackPressedListener,*/ SearchView.OnQueryTextListener, ChildTabFragment, Serializable {
    public static String SELECT_ROUTE = "select_route";

    private SearchView svSearchRoutes;
    private ImageButton ibDone;
    private ProgressBar pbRoutesList;
    private ListView lvResultRoutes;

    private ArrayList<Route> alRoutes = null;
    private RoutesListAdapter adapter;
    private FragmentManager myFragmentManager;
    private String actionBarTitle = "";

    private View view;

    public RoutesListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if ( view != null ) {
            if ( (ViewGroup)view.getParent() != null ) {
                ((ViewGroup) view.getParent()).removeView(view);
            }
            return view;
        }

        view = inflater.inflate(R.layout.fragment_routes_list, container, false);

        svSearchRoutes = (SearchView) view.findViewById(R.id.svSearchRoutes);

        svSearchRoutes.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ibDone.setVisibility(View.VISIBLE);
                } else {
                    ibDone.setVisibility(View.GONE);
                }
            }
        });

        svSearchRoutes.setIconifiedByDefault(false);

        ibDone = (ImageButton) view.findViewById(R.id.ibDoneRoutes);
        ibDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });

        pbRoutesList = (ProgressBar) view.findViewById(R.id.pbRoutesList);
        lvResultRoutes = (ListView) view.findViewById(R.id.lvResultRoutes);

        actionBarTitle = getResources().getString(R.string.fragment_routes_tab_title);

        /*Новый вариант*/
        alRoutes = new ArrayList<>(MainActivity.getAlRoutes());
        adapter = new RoutesListAdapter(getActivity().getApplicationContext(), alRoutes);
        lvResultRoutes.setAdapter(adapter);

        myFragmentManager = this.getParentFragment().getChildFragmentManager();

        lvResultRoutes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Route selectRoute = alRoutes.get(position);

                FragmentTransaction myFragmentTransaction = myFragmentManager.beginTransaction();

                RouteDirectionsFragment routesDirectionsFragment = new RouteDirectionsFragment();
                Bundle args = new Bundle();
                args.putSerializable(SELECT_ROUTE, selectRoute);
                routesDirectionsFragment.setArguments(args);

                int containerViewId = ((TabFragment)getParentFragment()).getIdContent();
                myFragmentTransaction.replace(containerViewId, routesDirectionsFragment);

                myFragmentTransaction.addToBackStack(null);
                myFragmentTransaction.commit();

                //закрыть клаву searchview
                hideKeyboard();

            }
        });

        svSearchRoutes.setInputType(InputType.TYPE_CLASS_NUMBER);
        svSearchRoutes.setOnQueryTextListener(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (RoutesTabFragment.getSettingRoutesView() != RoutesTabFragment.SETTING_SEARCH_FILTER /*&& llSearchRoutes.getVisibility() == View.VISIBLE*/) {
            ((RoutesTabFragment)getParentFragment()).replaceRoutesNumFilterListFragment();
        }

        if (getParentFragment().getUserVisibleHint()) {
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(actionBarTitle);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onCreateOptionsMenu(menu, inflater);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onPrepareOptionsMenu(menu);
            menu.findItem(R.id.action_refresh).setVisible(false);
            menu.findItem(R.id.action_add_bookmarks).setVisible(false);
            menu.findItem(R.id.action_view_map).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(true);
            ((MainActivity) getActivity()).setDisplayShowHome(false);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.filter(newText);
        lvResultRoutes.setSelectionAfterHeaderView();
        return false;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void asyncTaskExecute() {
        RoutesLoadTask rlt = new RoutesLoadTask();
        rlt.execute();
    }

    @Override
    public void hideKeyboard() {
        if (svSearchRoutes != null) {
            svSearchRoutes.clearFocus();
        }
    }

    public void setAlRoutes(ArrayList<Route> alRoutes) {
        this.alRoutes = alRoutes;
    }

    class RoutesLoadTask extends AsyncTask<Void, Void, Void> {

        RoutesLoadTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbRoutesList.setVisibility(View.VISIBLE);
            lvResultRoutes.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(Void... params) {

            JsonParser jsonParser = new JsonParser();

            try {
                alRoutes = jsonParser.getRoutes();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            adapter = new RoutesListAdapter(getActivity().getApplicationContext(), alRoutes);
            lvResultRoutes.setAdapter(adapter);

            pbRoutesList.setVisibility(View.GONE);
            lvResultRoutes.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public String getActionBarTitle() {
        return actionBarTitle;
    }

}
