package ru.bus55.fragments.routes;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Field;
import java.util.List;

import ru.bus55.R;
import ru.bus55.activity.MainActivity;
import ru.bus55.fragments.ChildTabFragment;
import ru.bus55.fragments.TabFragment;
import ru.bus55.fragments.settings.SettingsFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class RoutesTabFragment extends Fragment implements TabFragment {

    static public int SETTING_SEARCH_FILTER = 0;
    static public int SETTING_NUM_FILTER = 1;

    private FragmentManager myFragmentManager;
    private FragmentTransaction myFragmentTransaction;
    private ChildTabFragment firstChildTabFragment;

    private int settingRoutesView = 0;

    public RoutesTabFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myFragmentManager = getChildFragmentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_routes_tab, container, false);

        //Вид вкладки маршрутов в соотвествии с настройками 0 - новый вид, 1 - старый вид
        settingRoutesView = getSettingRoutesView();

        //новый вид владки маршруты
        if (settingRoutesView == 0) {
            replaceRoutesListFragment();
        }

        //старый вид владки маршруты
        if (settingRoutesView == 1) {
            replaceRoutesNumFilterListFragment();
        }

        return view;

    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public String getTitle() {
        return MainActivity.getContext().getResources().getString(R.string.fragment_routes_tab_title);
    }

    @Override
    public int getIdContent() {
        return R.id.contentRoutesTab;
    }

    @Override
    public Fragment getCurrentFragment(){
        Fragment currentFragment = null;
        try {
            currentFragment = myFragmentManager.findFragmentById(getIdContent());
        } catch (NullPointerException e){
            currentFragment = null;
        }

        return currentFragment;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    //for java.lang.IllegalStateException: Activity has been destroyed
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ChildTabFragment getFirstChildTabFragment() {
        return firstChildTabFragment;
    }

    public List<Fragment> getListChildFragments(){
        return myFragmentManager.getFragments();
    }

    public void replaceRoutesListFragment() {

        myFragmentTransaction = myFragmentManager.beginTransaction();
        RoutesListFragment routesListFragment = new RoutesListFragment();
        firstChildTabFragment = routesListFragment;
        myFragmentTransaction.replace(R.id.contentRoutesTab, routesListFragment);
        myFragmentTransaction.commit();
    }

    public void replaceRoutesNumFilterListFragment() {
        myFragmentTransaction = myFragmentManager.beginTransaction();
        RoutesNumFilterListFragment routesNumFilterListFragment = new RoutesNumFilterListFragment();
        firstChildTabFragment = routesNumFilterListFragment;
        myFragmentTransaction.replace(R.id.contentRoutesTab, routesNumFilterListFragment);
        myFragmentTransaction.commit();
    }

    static public int getSettingRoutesView(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.getContext());
        return Integer.valueOf(preferences.getString(SettingsFragment.PREF_KEY_ROUTES_VIEW, "0"));
    }

}
