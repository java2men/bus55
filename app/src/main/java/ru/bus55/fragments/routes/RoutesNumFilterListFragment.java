package ru.bus55.fragments.routes;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.io.Serializable;
import java.util.ArrayList;

import ru.bus55.R;
import ru.bus55.activity.MainActivity;
import ru.bus55.adapters.RoutesNumFilterListAdapter;
import ru.bus55.fragments.ChildTabFragment;
import ru.bus55.fragments.TabFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class RoutesNumFilterListFragment extends Fragment implements ChildTabFragment, Serializable {

    public static String SELECT_NUM_FILTER = "select_num_filter";

    private View view;

    private ProgressBar pbRoutesNumFilterList;
    private ListView lvResultRoutesNumFilter;

    private ArrayList<String> alNumbers = null;

    private RoutesNumFilterListAdapter adapter;
    private FragmentManager myFragmentManager;
    private String actionBarTitle = "";

    public RoutesNumFilterListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if ( view != null ) {
            if ( (ViewGroup)view.getParent() != null ) {
                ((ViewGroup) view.getParent()).removeView(view);
            }
            return view;
        }

        view = inflater.inflate(R.layout.fragment_routes_num_filter_list, container, false);

        pbRoutesNumFilterList = (ProgressBar) view.findViewById(R.id.pbRoutesNumFilterList);
        lvResultRoutesNumFilter = (ListView) view.findViewById(R.id.lvResultRoutesNumFilter);

        myFragmentManager = this.getParentFragment().getChildFragmentManager();
        actionBarTitle = getResources().getString(R.string.fragment_routes_tab_title);

        alNumbers = new ArrayList<>();

        for(int i=1; i<=9; i++) {
            alNumbers.add(new StringBuilder("").append(i).append("x").toString());
        }

        adapter = new RoutesNumFilterListAdapter(getActivity().getApplicationContext(), alNumbers);
        lvResultRoutesNumFilter.setAdapter(adapter);
        lvResultRoutesNumFilter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectNumFilter = String.valueOf(position + 1);

                FragmentTransaction myFragmentTransaction = myFragmentManager.beginTransaction();

                RoutesNumFilteredListFragment routesNumFilteredListFragment = new RoutesNumFilteredListFragment();
                Bundle args = new Bundle();
                args.putString(SELECT_NUM_FILTER, selectNumFilter);
                routesNumFilteredListFragment.setArguments(args);

                int containerViewId = ((TabFragment)getParentFragment()).getIdContent();
                myFragmentTransaction.replace(containerViewId, routesNumFilteredListFragment);

                myFragmentTransaction.addToBackStack(null);
                myFragmentTransaction.commit();

            }
        });

        return view;
    }


    @Override
    public void asyncTaskExecute() {

    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void onResume() {
        super.onResume();

        //Проверить настройки, если не совпадает то заменить
        if (RoutesTabFragment.getSettingRoutesView() != RoutesTabFragment.SETTING_NUM_FILTER) {
            ((RoutesTabFragment)getParentFragment()).replaceRoutesListFragment();
        }

        if (getParentFragment().getUserVisibleHint()) {
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(actionBarTitle);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onCreateOptionsMenu(menu, inflater);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onPrepareOptionsMenu(menu);
            menu.findItem(R.id.action_refresh).setVisible(false);
            menu.findItem(R.id.action_add_bookmarks).setVisible(false);
            menu.findItem(R.id.action_view_map).setVisible(false);
            menu.findItem(R.id.action_settings).setVisible(true);
            ((MainActivity) getActivity()).setDisplayShowHome(false);
        }
    }

    @Override
    public String getActionBarTitle() {
        return actionBarTitle;
    }
}
