package ru.bus55.fragments.routes;


import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;

import ru.bus55.R;
import ru.bus55.activity.MainActivity;
import ru.bus55.adapters.StationsByRouteAdapter;
import ru.bus55.fragments.ChildTabFragment;
import ru.bus55.fragments.TabFragment;
import ru.bus55.fragments.bookmarks.BookmarksListFragment;
import ru.bus55.fragments.stations.PredictionsStationFragment;
import ru.bus55.fragments.stations.StationsListFragment;
import ru.bus55.models.Direction;
import ru.bus55.models.InfoNextStation;
import ru.bus55.models.JsonParser;
import ru.bus55.models.Route;
import ru.bus55.models.Station;

/**
 * A simple {@link Fragment} subclass.
 */
public class RouteDirectionsFragment extends Fragment implements MainActivity.OnBackPressedListener, ChildTabFragment {

    public static String AL_ROUTE_DIRECTIONS = "al_route_directions";

    private FragmentTabHost fthRoutesDiretions;
    private FrameLayout flRouteDiretionsContent;

    private ProgressBar pbRouteDirections;
    private LinearLayout llRoute;

    private TabLayout tlRoute;
    private ListView lvRouteAB;
    private ListView lvRouteBA;
    private StationsByRouteAdapter adapterAB;
    private StationsByRouteAdapter adapterBA;

    private ArrayList<Station> alAB;
    private ArrayList<Station> alBA;
    private Direction selectDirection;

    private Route selectRoute;
    private FragmentManager myFragmentManager;
    private ArrayList<Station> alRouteStation;

    private String actionBarTitle = "";

    private View view;

    public RouteDirectionsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if ( view != null ) {
            if ( (ViewGroup)view.getParent() != null ) {
                ((ViewGroup) view.getParent()).removeView(view);
            }
            return view;
        }

        view = inflater.inflate(R.layout.fragment_route_directions, container, false);

        tlRoute = (TabLayout) view.findViewById(R.id.tlRoute);

        llRoute = (LinearLayout) view.findViewById(R.id.llRoute);

        lvRouteAB = (ListView) view.findViewById(R.id.lvRouteAB);
        lvRouteBA = (ListView) view.findViewById(R.id.lvRouteBA);
        pbRouteDirections = (ProgressBar) view.findViewById(R.id.pbRouteDirections);

        selectRoute = (Route) getArguments().get(RoutesListFragment.SELECT_ROUTE);

        StringBuilder sbType = new StringBuilder(selectRoute.getName());

        if (selectRoute.getType().equals(JsonParser.TYPE_BUS)) {
            sbType.append(" автобус");
        }

        if (selectRoute.getType().equals(JsonParser.TYPE_TRAM)) {
            sbType.append(" трамвай");
        }

        if (selectRoute.getType().equals(JsonParser.TYPE_TROLLEYBUS)) {
            sbType.append(" троллейбус");
        }

        if (selectRoute.getType().equals(JsonParser.TYPE_TAXI)) {
            sbType.append(" маршрутное такси");
        }

        actionBarTitle = sbType.toString();

        myFragmentManager = this.getParentFragment().getChildFragmentManager();

        tlRoute.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    lvRouteAB.setVisibility(View.VISIBLE);
                }

                if (tab.getPosition() == 1) {
                    lvRouteBA.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    lvRouteAB.setVisibility(View.GONE);
                }

                if (tab.getPosition() == 1) {
                    lvRouteBA.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        lvRouteAB.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Station selectStation = alAB.get(position);

                FragmentTransaction myFragmentTransaction = myFragmentManager.beginTransaction();

                PredictionsStationFragment predictionsStationFragment = new PredictionsStationFragment();

                Bundle args = new Bundle();
                args.putSerializable(StationsListFragment.SELECT_STATION, selectStation);
                predictionsStationFragment.setArguments(args);

                int containerViewId = ((TabFragment)getParentFragment()).getIdContent();
                myFragmentTransaction.replace(containerViewId, predictionsStationFragment);

                myFragmentTransaction.addToBackStack(null);
                myFragmentTransaction.commit();
            }
        });

        lvRouteBA.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Station selectStation = alBA.get(position);

                FragmentTransaction myFragmentTransaction = myFragmentManager.beginTransaction();

                PredictionsStationFragment predictionsStationFragment = new PredictionsStationFragment();

                Bundle args = new Bundle();
                args.putSerializable(StationsListFragment.SELECT_STATION, selectStation);
                predictionsStationFragment.setArguments(args);

                int containerViewId = ((TabFragment)getParentFragment()).getIdContent();
                myFragmentTransaction.replace(containerViewId, predictionsStationFragment);

                myFragmentTransaction.addToBackStack(null);
                myFragmentTransaction.commit();
            }
        });


        //Получаем данные или восстаналиваем, если существуют
        if ((alRouteStation == null) || (alRouteStation.isEmpty())) {
            asyncTaskExecute();
        } else {

            buildTlRoute();

            pbRouteDirections.setVisibility(View.GONE);
            llRoute.setVisibility(View.VISIBLE);

        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getParentFragment().getUserVisibleHint()) {
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(actionBarTitle);

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onCreateOptionsMenu(menu, inflater);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (getParentFragment().getUserVisibleHint()) {
            super.onPrepareOptionsMenu(menu);

            menu.findItem(R.id.action_refresh).setVisible(false);
            menu.findItem(R.id.action_add_bookmarks).setVisible(true);
            menu.findItem(R.id.action_view_map).setVisible(true);
            menu.findItem(R.id.action_settings).setVisible(true);

            ((MainActivity) getActivity()).setDisplayShowHome(true);
        }
    }

    @Override
    public void onBackPressed() {
        myFragmentManager.popBackStack();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void asyncTaskExecute() {
        StationsByRouteLoadTask sbrlt = new StationsByRouteLoadTask();
        sbrlt.execute();
    }

    @Override
    public void hideKeyboard() {

    }

    public void setRouteMapFragment() {

        FragmentTransaction myFragmentTransaction = myFragmentManager.beginTransaction();
        RouteMapFragment routeMapFragment = new RouteMapFragment();
        Bundle args = new Bundle();
        args.putSerializable(RoutesListFragment.SELECT_ROUTE, selectRoute);
        routeMapFragment.setArguments(args);

        int containerViewId = ((TabFragment)getParentFragment()).getIdContent();
        myFragmentTransaction.replace(containerViewId, routeMapFragment);

        myFragmentTransaction.addToBackStack(null);
        myFragmentTransaction.commit();
    }

    public void addRouteToBookmark() {
        BookmarksListFragment bookmarksListFragment = ((BookmarksListFragment)((MainActivity)getActivity()).findFragment(BookmarksListFragment.class));
        bookmarksListFragment.addRouteToBookmark(selectRoute);
    }

    class StationsByRouteLoadTask extends AsyncTask<Void, Void, Void> {

        private boolean isError = false;

        StationsByRouteLoadTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbRouteDirections.setVisibility(View.VISIBLE);
            llRoute.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                JsonParser jsonParser = new JsonParser();

                alRouteStation = jsonParser.getStationsByRoute(selectRoute.getId());
                alAB = new ArrayList<>();
                alBA = new ArrayList<>();
                Station prevAB = null;
                Station prevBA = null;
                for(Station s : alRouteStation) {
                    if (s.getDir().equals("1")) {
                        alAB.add(s);
                        if (prevAB != null) {
                            //Остановки в маршрутах уже точные, поэтому idStation1 это и есть id остановки s, idStation2 - это id следующей остановки
                            prevAB.setInfoNextStation(new InfoNextStation(prevAB.getIdStation(), s.getIdStation(), s.getName()));
                        }
                        prevAB = s;
                        continue;
                    }

                    if (s.getDir().equals("0")) {
                        alBA.add(s);
                        if (prevBA != null) {
                            //Остановки в маршрутах уже точные, поэтому idStation1 это и есть id остановки s, idStation2 - это id следующей остановки
                            prevBA.setInfoNextStation(new InfoNextStation(prevBA.getIdStation(), s.getIdStation(), s.getName()));
                        }
                        prevBA = s;
                        continue;
                    }
                }
            } catch (Exception e) {
                isError = true;
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (isError) {
                pbRouteDirections.setVisibility(View.GONE);
                llRoute.setVisibility(View.GONE);

                AlertDialog.Builder ad = new AlertDialog.Builder(MainActivity.getContext());

                ad.setTitle("Ошибка");  // заголовок
                ad.setMessage("Ошибка загрузки данных."); // сообщение
                ad.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        onBackPressed();
                    }
                });

                ad.setCancelable(true);
                ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        onBackPressed();
                    }
                });

                ad.show();


            } else {

                buildTlRoute();

                pbRouteDirections.setVisibility(View.GONE);
                llRoute.setVisibility(View.VISIBLE);
            }

        }
    }

    private void buildTlRoute() {
        tlRoute.addTab(tlRoute.newTab().setText("от " + alAB.get(0).getName()), 0);
        if (alBA != null && alBA.size() != 0) {
            tlRoute.addTab(tlRoute.newTab().setText("от " + alBA.get(0).getName()), 1);
        } else {
            tlRoute.addTab(tlRoute.newTab().setText("Отсутствует "), 1);
        }

        if (lvRouteAB.getAdapter() == null) {
            adapterAB = new StationsByRouteAdapter(MainActivity.getContext(), alAB);
            lvRouteAB.setAdapter(adapterAB);
        } else {
            adapterAB.getList().clear();
            adapterAB.getList().addAll(alAB);
            adapterAB.notifyDataSetChanged();
        }

        //если приложение открылось и адаптер не создан
        if (lvRouteBA.getAdapter() == null) {
            adapterBA = new StationsByRouteAdapter(MainActivity.getContext(), alBA);
            lvRouteBA.setAdapter(adapterBA);
        } else {
            adapterBA.getList().clear();
            adapterBA.getList().addAll(alBA);
            adapterBA.notifyDataSetChanged();
        }

        //Сделать селект, чтобы listView показать, если  сделать tlRoute.getTabAt(0).select(), то событие селекта не срабатывает
        if (tlRoute.getTabCount() > 1) {
            tlRoute.getTabAt(1).select();
            tlRoute.getTabAt(0).select();
        }
    }

    @Override
    public String getActionBarTitle() {
        return actionBarTitle;
    }

}
