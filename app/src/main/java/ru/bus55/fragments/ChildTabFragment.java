package ru.bus55.fragments;

/**
 * Created by IALozhnikov on 16.05.2016.
 */
public interface ChildTabFragment {

    public void asyncTaskExecute();

    public void hideKeyboard();

    public String getActionBarTitle();

}
