package ru.bus55.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.bus55.activity.MainActivity;
import ru.bus55.R;
import ru.bus55.fragments.bookmarks.BookmarksListFragment;
import ru.bus55.models.Station;

/**
 * Created by IALozhnikov on 10.06.2016.
 */
public class BookmarkStationListAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private List<Station> list = null;
    private ArrayList<Station> originalList;

    public BookmarkStationListAdapter(Context context, List<Station> list) {
        mContext = context;
        this.list = new ArrayList<>();
        this.list.addAll(list);
        inflater = LayoutInflater.from(mContext);
        this.originalList = new ArrayList<Station>();
        this.originalList.addAll(list);
    }

    public class ViewHolder {
        ImageView type;
        TextView nameStation;
        TextView nameDirection;
        ImageButton remove;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Station getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<Station> getList() {
        return list;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_bookmark_stations_list, null);
            holder.nameStation = (TextView) view.findViewById(R.id.name_bookmark_station);
            holder.nameDirection = (TextView) view.findViewById(R.id.name_bookmark_direction);
            holder.remove = (ImageButton) view.findViewById(R.id.remove_bookmark_station);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Station station = list.get(position);
        holder.nameStation.setText(station.getName());
        holder.nameDirection.setText(station.getInfoNextStation().getStationName2());

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity mainActivity = (MainActivity)MainActivity.getContext();
                BookmarksListFragment bookmarksListFragment = (BookmarksListFragment) mainActivity.findFragment(BookmarksListFragment.class);
                Station selectStation = list.get(position);
                bookmarksListFragment.removeStationFromBookmark(selectStation);
            }
        });

        return view;
    }

}
