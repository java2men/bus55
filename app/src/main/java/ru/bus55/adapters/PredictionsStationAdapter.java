package ru.bus55.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.bus55.R;
import ru.bus55.activity.MainActivity;
import ru.bus55.models.JsonParser;
import ru.bus55.models.Prediction;

/**
 * Created by IALozhnikov on 04.05.2016.
 */
public class PredictionsStationAdapter extends BaseAdapter{

    Context mContext;
    LayoutInflater inflater;
    private List<Prediction> list = null;
    private ArrayList<Prediction> originalList;

    public PredictionsStationAdapter(Context context, List<Prediction> list) {
        mContext = context;
        this.list = list;
        inflater = LayoutInflater.from(mContext);
        this.originalList = new ArrayList<Prediction>();
        this.originalList.addAll(list);
    }

    public class ViewHolder {
        ImageView type;
        TextView namePrediction;
        TextView firstLast;
        TextView timeAvg;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Prediction getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_directions_content, null);
            holder.type = (ImageView) view.findViewById(R.id.type_direction_content);
            holder.namePrediction = (TextView) view.findViewById(R.id.name_prediction);
            holder.firstLast = (TextView) view.findViewById(R.id.first_last);
            holder.timeAvg = (TextView) view.findViewById(R.id.time_avg);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Prediction prediction = list.get(position);
        if (prediction.getType().equals(JsonParser.TYPE_BUS)) {
            holder.type.setImageResource(R.drawable.ic_bus);
        } else if (prediction.getType().equals(JsonParser.TYPE_TROLLEYBUS)) {
            holder.type.setImageResource(R.drawable.ic_trolleybus);
        } else if (prediction.getType().equals(JsonParser.TYPE_TRAM)) {
            holder.type.setImageResource(R.drawable.ic_tram);
        } else if (prediction.getType().equals(JsonParser.TYPE_TAXI)) {
            holder.type.setImageResource(R.drawable.ic_taxi);
        }

        holder.namePrediction.setText(prediction.getName());
        holder.firstLast.setText(new StringBuilder(prediction.getFirst()).append(" - ").append(prediction.getLast()) );
        holder.timeAvg.setText(prediction.getTimeAvg());

        if (holder.timeAvg.getText().toString().equals("прибытие")) {
            holder.timeAvg.setBackgroundColor(ContextCompat.getColor(MainActivity.getContext(), R.color.colorComing));
        } else {
            holder.timeAvg.setBackgroundColor(ContextCompat.getColor(MainActivity.getContext(), R.color.colorPrimary));
        }

        return view;
    }

}
