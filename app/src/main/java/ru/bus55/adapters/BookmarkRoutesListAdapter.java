package ru.bus55.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.bus55.activity.MainActivity;
import ru.bus55.R;
import ru.bus55.fragments.bookmarks.BookmarksListFragment;
import ru.bus55.models.JsonParser;
import ru.bus55.models.Route;

/**
 * Created by IALozhnikov on 10.06.2016.
 */
public class BookmarkRoutesListAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private List<Route> list = null;
    private ArrayList<Route> originalList;

    public BookmarkRoutesListAdapter(Context context, List<Route> list) {
        mContext = context;
        this.list = new ArrayList<>();
        this.list.addAll(list);
        inflater = LayoutInflater.from(mContext);
        this.originalList = new ArrayList<Route>();
        this.originalList.addAll(list);
    }

    public class ViewHolder {
        ImageView type;
        TextView name;
        ImageButton remove;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Route getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<Route> getList() {
        return list;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_bookmark_routes_list, null);
            holder.type = (ImageView) view.findViewById(R.id.type_bookmark_route);
            holder.name = (TextView) view.findViewById(R.id.name_bookmark_route);
            holder.remove = (ImageButton) view.findViewById(R.id.remove_bookmark_route);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Route route = list.get(position);
        if (route.getType().equals(JsonParser.TYPE_BUS)) {
            holder.type.setImageResource(R.drawable.ic_bus);
        } else if (route.getType().equals(JsonParser.TYPE_TROLLEYBUS)) {
            holder.type.setImageResource(R.drawable.ic_trolleybus);
        } else if (route.getType().equals(JsonParser.TYPE_TRAM)) {
            holder.type.setImageResource(R.drawable.ic_tram);
        } else if (route.getType().equals(JsonParser.TYPE_TAXI)) {
            holder.type.setImageResource(R.drawable.ic_taxi);
        }

        holder.name.setText(route.getName());

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity mainActivity = (MainActivity)MainActivity.getContext();
                BookmarksListFragment bookmarksListFragment = (BookmarksListFragment) mainActivity.findFragment(BookmarksListFragment.class);
                Route selectRoute = list.get(position);
                bookmarksListFragment.removeRouteFromBookmark(selectRoute);
            }
        });

        return view;
    }



}
