package ru.bus55.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.bus55.R;
import ru.bus55.models.Direction;

/**
 * Created by IALozhnikov on 04.05.2016.
 */
public class DirectionsListAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private List<Direction> list = null;
    private ArrayList<Direction> originalList;

    public DirectionsListAdapter(Context context, List<Direction> list) {
        mContext = context;
        this.list = list;
        inflater = LayoutInflater.from(mContext);
        this.originalList = new ArrayList<Direction>();
        this.originalList.addAll(list);
    }

    public class ViewHolder {
        TextView nameDirection;
        TextView routesDirection;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Direction getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_directions_list, null);
            holder.nameDirection = (TextView) view.findViewById(R.id.name_direction);
            holder.routesDirection = (TextView) view.findViewById(R.id.routes_direction);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.nameDirection.setText(list.get(position).getStationName2());
        holder.routesDirection.setText(list.get(position).getRoutes());

        return view;
    }

}
