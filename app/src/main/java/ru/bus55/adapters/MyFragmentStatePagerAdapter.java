package ru.bus55.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ru.bus55.fragments.bookmarks.BookmarksTabFragment;
import ru.bus55.fragments.map.MapTabFragment;
import ru.bus55.fragments.routes.RoutesTabFragment;
import ru.bus55.fragments.stations.StationsTabFragment;

/**
 * Created by IALozhnikov on 25.04.2016.
 */
public class MyFragmentStatePagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;
    RoutesTabFragment routesTabFragment = null;
    StationsTabFragment stationsTabFragment = null;
    BookmarksTabFragment bookmarksTabFragment = null;
    MapTabFragment mapTabFragment = null;

    public MyFragmentStatePagerAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.mNumOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                if (routesTabFragment == null) {
                    routesTabFragment = new RoutesTabFragment();
                }
                return routesTabFragment;
            case 1:
                if (stationsTabFragment == null) {
                    stationsTabFragment = new StationsTabFragment();
                }
                return stationsTabFragment;
            case 2:
                if (bookmarksTabFragment == null) {
                    bookmarksTabFragment = new BookmarksTabFragment();
                }
                return bookmarksTabFragment;
            case 3:
                if (mapTabFragment == null) {
                    mapTabFragment = new MapTabFragment();
                }
                return mapTabFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}
