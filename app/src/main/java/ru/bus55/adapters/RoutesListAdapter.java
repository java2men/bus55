package ru.bus55.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ru.bus55.R;
import ru.bus55.models.JsonParser;
import ru.bus55.models.Route;

/**
 * Created by IALozhnikov on 17.05.2016.
 */
public class RoutesListAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private List<Route> list = null;
    private ArrayList<Route> originalList;

    public RoutesListAdapter(Context context, List<Route> list) {
        mContext = context;
        this.list = list;
        inflater = LayoutInflater.from(mContext);
        this.originalList = new ArrayList<Route>();
        this.originalList.addAll(list);
    }

    public class ViewHolder {
        ImageView type;
        TextView name;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Route getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_routes_list, null);
            holder.type = (ImageView) view.findViewById(R.id.type_route);
            holder.name = (TextView) view.findViewById(R.id.name_route);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Route route = list.get(position);
        if (route.getType().equals(JsonParser.TYPE_BUS)) {
            holder.type.setImageResource(R.drawable.ic_bus);
        } else if (route.getType().equals(JsonParser.TYPE_TROLLEYBUS)) {
            holder.type.setImageResource(R.drawable.ic_trolleybus);
        } else if (route.getType().equals(JsonParser.TYPE_TRAM)) {
            holder.type.setImageResource(R.drawable.ic_tram);
        } else if (route.getType().equals(JsonParser.TYPE_TAXI)) {
            holder.type.setImageResource(R.drawable.ic_taxi);
        }

        holder.name.setText(route.getName());
        return view;
    }

    // Filter Class
    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(originalList);
        }
        else {
            for (Route wp : originalList) {
                if (wp.getName().toLowerCase(Locale.getDefault()).startsWith(charText)) {
                    list.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}
