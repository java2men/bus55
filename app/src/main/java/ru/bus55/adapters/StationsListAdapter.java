package ru.bus55.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ru.bus55.R;
import ru.bus55.models.Station;

/**
 * Created by IALozhnikov on 29.04.2016.
 */
public class StationsListAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private List<Station> list = null;
    private ArrayList<Station> originalList;

    public StationsListAdapter(Context context, List<Station> list) {
        mContext = context;
        this.list = list;
        inflater = LayoutInflater.from(mContext);
        this.originalList = new ArrayList<Station>();
        this.originalList.addAll(list);
    }

    public class ViewHolder {
        TextView nameStation;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Station getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_stations_list, null);

            holder.nameStation = (TextView) view.findViewById(R.id.name_station);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.nameStation.setText(list.get(position).getName());

        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(originalList);
        }
        else {
            for (Station wp : originalList) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    list.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}
