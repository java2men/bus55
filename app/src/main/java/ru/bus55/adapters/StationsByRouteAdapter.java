package ru.bus55.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.bus55.R;
import ru.bus55.models.Station;

/**
 * Created by IALozhnikov on 29.04.2016.
 */
public class StationsByRouteAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private List<Station> list = null;
    private ArrayList<Station> originalList;

    public StationsByRouteAdapter(Context context, List<Station> list) {
        mContext = context;
        this.list = list;
        inflater = LayoutInflater.from(mContext);
    }

    public class ViewHolder {
        TextView nameStation;
        TextView timeAvg;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Station getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<Station> getList() {
        return list;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_stations_by_route_list, null);

            holder.nameStation = (TextView) view.findViewById(R.id.name_station);
            holder.timeAvg = (TextView) view.findViewById(R.id.time_avg_by_route);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.nameStation.setText(list.get(position).getName());
        holder.timeAvg.setText("");//откл.

        return view;
    }

}
