package ru.bus55.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ru.bus55.R;

/**
 * Created by IALozhnikov on 17.05.2016.
 */
public class RoutesNumFilterListAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private List<String> list = null;

    public RoutesNumFilterListAdapter(Context context, List<String> list) {
        mContext = context;
        this.list = list;
        inflater = LayoutInflater.from(mContext);
    }

    public class ViewHolder {
        ImageView type;
        TextView name;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public String getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_routes_num_filter_list, null);
            holder.name = (TextView) view.findViewById(R.id.name_num_filter_route);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        String number = list.get(position);
        holder.name.setText(number);

        return view;
    }

}
