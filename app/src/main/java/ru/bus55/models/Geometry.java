package ru.bus55.models;

import java.util.ArrayList;

/**
 * Created by IALozhnikov on 01.06.2016.
 */
public class Geometry {

    private String type;
    private ArrayList<Coordinates> coordinates;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<Coordinates> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<Coordinates> coordinates) {
        this.coordinates = coordinates;
    }
}
