package ru.bus55.models;

/**
 * Created by IALozhnikov on 01.06.2016.
 */
public class Coordinates {

    private String lon;
    private String lat;

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }
}
