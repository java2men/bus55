package ru.bus55.models;

/**
 * Created by IALozhnikov on 27.04.2016.
 */
public class Prediction {

    private String id;
    private String name;
    private String type;
    private String first;
    private String last;
    private String firstId;
    private String lastId;
    private String timeAvg;
    private String timeMin;
    private String isRasp;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getFirstId() {
        return firstId;
    }

    public void setFirstId(String firstId) {
        this.firstId = firstId;
    }

    public String getLastId() {
        return lastId;
    }

    public void setLastId(String lastId) {
        this.lastId = lastId;
    }

    public String getTimeAvg() {
        return timeAvg;
    }

    public void setTimeAvg(String timeAvg) {
        this.timeAvg = timeAvg;
    }

    public String getTimeMin() {
        return timeMin;
    }

    public void setTimeMin(String timeMin) {
        this.timeMin = timeMin;
    }

    public String getIsRasp() {
        return isRasp;
    }

    public void setIsRasp(String isRasp) {
        this.isRasp = isRasp;
    }
}
