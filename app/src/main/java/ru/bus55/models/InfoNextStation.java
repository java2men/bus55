package ru.bus55.models;

import java.io.Serializable;

/**
 * Created by IALozhnikov on 29.06.2016.
 */
public class InfoNextStation implements Serializable {
    private String idStation1;
    private String idStation2;
    private String stationName2;

    public InfoNextStation(){}

    public InfoNextStation(String idStation1, String idStation2, String stationName2) {
        this.idStation1 = idStation1;
        this.idStation2 = idStation2;
        this.stationName2 = stationName2;
    }

    public String getIdStation1() {
        return idStation1;
    }

    public void setIdStation1(String idStation1) {
        this.idStation1 = idStation1;
    }

    public String getIdStation2() {
        return idStation2;
    }

    public void setIdStation2(String idStation2) {
        this.idStation2 = idStation2;
    }

    public String getStationName2() {
        return stationName2;
    }

    public void setStationName2(String stationName2) {
        this.stationName2 = stationName2;
    }
}
