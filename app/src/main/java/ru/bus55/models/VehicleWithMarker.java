package ru.bus55.models;

import com.google.android.gms.maps.model.Marker;

/**
 * Created by ilya on 26.07.16.
 */
public class VehicleWithMarker {

    private Vehicle vehicle;
    private Marker marker;

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }


}
