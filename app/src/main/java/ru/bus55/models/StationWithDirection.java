package ru.bus55.models;

import java.io.Serializable;

/**
 * Created by IALozhnikov on 16.06.2016.
 */
public class StationWithDirection implements Serializable {

    private Station station;
    private Direction direction;

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

}
