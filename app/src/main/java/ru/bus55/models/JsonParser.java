package ru.bus55.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.bus55.utils.WebRequest;

/**
 * Created by IALozhnikov on 25.04.2016.
 */
public class JsonParser {

    private static String GET_ROUTES_URL_JSON = "http://t.bus55.ru/index.php/appjson/get_routes";
    private static String GET_STATIONS_URL_JSON = "http://t.bus55.ru/index.php/appjson/get_stations";
    private static String GET_STATIONS_BY_ROUTE_URL_JSON = "http://t.bus55.ru/index.php/appjson/get_stations_by_route/";
    private static String GET_DIRECTIONS_URL_JSON = "http://t.bus55.ru/index.php/appjson/get_dir/";
    private static String GET_PREDICTIONS_URL_JSON = "http://t.bus55.ru/index.php/appjson/get_predict/";
    private static String GET_GEOLOC_STATIONS_URL_JSON = "http://t.bus55.ru/index.php/appjson/get_stations_geoloc/";
    private static String GET_ROUTECOL_GEO_URL_JSON = "http://bus.admomsk.ru/index.php/getroute/routecol_geo/";
    private static String GET_BUS_URL_JSON = "http://bus.admomsk.ru/index.php/getroute/getbus/";

    public static String TYPE_BUS = "bus";
    public static String TYPE_TROLLEYBUS = "trol";
    public static String TYPE_TRAM = "tram";
    public static String TYPE_TAXI = "taxi";

    public ArrayList<Route> getRoutes() throws JSONException {
        WebRequest webRequest = new WebRequest();
        String data = webRequest.makeWebServiceCall(GET_ROUTES_URL_JSON, WebRequest.GETRequest);
        JSONObject jsonObject = new JSONObject(data);
        JSONArray jsonArray = jsonObject.getJSONArray("routes");
        ArrayList<Route> alRoutes = new ArrayList<>();

        for(int i=0; i<jsonArray.length(); i++) {

            JSONObject joRoute = jsonArray.getJSONObject(i);
            Route route = new Route();
            route.setId(joRoute.getString("id"));
            route.setId2gis(joRoute.getString("id_2gis"));
            route.setName(joRoute.getString("name"));;
            route.setType(joRoute.getString("type"));
            route.setFirst(joRoute.getString("first"));
            route.setLast(joRoute.getString("last"));
            route.setFirstId(joRoute.getString("first_id"));
            route.setLastId(joRoute.getString("last_id"));
            route.setStationsString(joRoute.getString("stations_string"));
            route.setStationsStringDir1(joRoute.getString("stations_string_dir1"));

            alRoutes.add(route);
        }

        return alRoutes;
    }

    public ArrayList<Station> getStations() throws JSONException {
        WebRequest webRequest = new WebRequest();
        String data = webRequest.makeWebServiceCall(GET_STATIONS_URL_JSON, WebRequest.GETRequest);

        JSONArray jsonArray = new JSONArray(data);
        ArrayList<Station> alStations = new ArrayList<>();

        for(int i=0; i<jsonArray.length(); i++) {

            JSONObject joStation = jsonArray.getJSONObject(i);
            Station station = new Station();
            station.setId(joStation.getString("id"));
            station.setLon(joStation.getString("lon"));
            station.setLat(joStation.getString("lat"));
            station.setName(joStation.getString("name"));
            station.setId2gis(joStation.getString("id_2gis"));

            alStations.add(station);
        }

        return alStations;
    }

    public ArrayList<Station> getStationsByRoute(String routeId) throws JSONException {
        WebRequest webRequest = new WebRequest();
        String data = webRequest.makeWebServiceCall(new StringBuilder(GET_STATIONS_BY_ROUTE_URL_JSON).append(routeId).toString(), WebRequest.GETRequest);

        JSONObject jsonObject = new JSONObject(data);
        JSONArray jsonArray = jsonObject.getJSONArray("stations");
        ArrayList<Station> alStations = new ArrayList<>();

        for(int i=0; i<jsonArray.length(); i++) {

            JSONObject joStation = jsonArray.getJSONObject(i);
            Station station = new Station();
            station.setId(joStation.getString("id"));
            station.setIdRoute(joStation.getString("id_route"));
            station.setOrder(joStation.getString("order"));
            station.setIdStation(joStation.getString("id_station"));
            station.setDir(joStation.getString("dir"));
            station.setName(joStation.getString("name"));

            alStations.add(station);
        }

        return alStations;
    }

    public ArrayList<Direction> getDirections(String stationId) throws JSONException {
        WebRequest webRequest = new WebRequest();
        String data = webRequest.makeWebServiceCall(new StringBuilder(GET_DIRECTIONS_URL_JSON).append(stationId).toString(), WebRequest.GETRequest);
        JSONObject jsonObject = new JSONObject(data);
        JSONArray jsonArray = jsonObject.getJSONArray("graph");
        ArrayList<Direction> alDirections = new ArrayList<>();

        for(int i=0; i<jsonArray.length(); i++) {

            JSONObject joDirection = jsonArray.getJSONObject(i);
            Direction direction = new Direction();
            direction.setId(joDirection.getString("id"));
            direction.setIdStation1(joDirection.getString("id_station_1"));
            direction.setIdStation2(joDirection.getString("id_station_2"));
            direction.setUp(joDirection.getString("up"));
            direction.setRoutes(joDirection.getString("routes"));
            direction.setStationName2(joDirection.getString("station_name_2"));

            alDirections.add(direction);
        }

        return alDirections;
    }

    public ArrayList<Prediction> getPredictions(String directionId) throws JSONException {
        WebRequest webRequest = new WebRequest();
        String data = webRequest.makeWebServiceCall(new StringBuilder(GET_PREDICTIONS_URL_JSON).append(directionId).toString(), WebRequest.GETRequest);
        JSONObject jsonObject = new JSONObject(data);
        JSONArray jsonArray = jsonObject.getJSONArray("results");
        ArrayList<Prediction> alPredictions = new ArrayList<>();

        for(int i=0; i<jsonArray.length(); i++) {

            JSONObject joPrediction = jsonArray.getJSONObject(i);

            Prediction prediction = new Prediction();
            prediction.setId(joPrediction.getString("id"));
            prediction.setName(joPrediction.getString("name"));
            prediction.setType(joPrediction.getString("type"));
            prediction.setFirst(joPrediction.getString("first"));
            prediction.setLast(joPrediction.getString("last"));
            prediction.setFirstId(joPrediction.getString("first_id"));
            prediction.setLastId(joPrediction.getString("last_id"));
            prediction.setTimeAvg(joPrediction.getString("time_avg"));
            prediction.setTimeMin(joPrediction.getString("time_min"));
            prediction.setIsRasp(joPrediction.getString("is_rasp"));

            alPredictions.add(prediction);
        }

        return alPredictions;
    }

    public ArrayList<Station> getGeolocStations(String lat, String lon) throws JSONException {
        WebRequest webRequest = new WebRequest();
        String data = webRequest.makeWebServiceCall(new StringBuilder(GET_GEOLOC_STATIONS_URL_JSON).append(lat).append("/").append(lon).toString(), WebRequest.GETRequest);
        JSONArray jsonArray = new JSONArray(data);
        ArrayList<Station> alGeolocStations = new ArrayList<>();

        for(int i=0; i<jsonArray.length(); i++) {

            JSONObject joGeolocStation = jsonArray.getJSONObject(i);
            Station geolocStation = new Station();
            geolocStation.setId(joGeolocStation.getString("id"));
            geolocStation.setName(joGeolocStation.getString("name"));
            geolocStation.setLon(joGeolocStation.getString("lon"));
            geolocStation.setLat(joGeolocStation.getString("lat"));

            InfoNextStation infoNextStation = null;
            if (!joGeolocStation.getString("dir").equals("[]")) {
                JSONObject joDir = joGeolocStation.getJSONObject("dir");
                infoNextStation = new InfoNextStation();
                infoNextStation.setIdStation1(joDir.getString("id_station_1"));
                infoNextStation.setIdStation2(joDir.getString("id_station_2"));
                infoNextStation.setStationName2(joDir.getString("station_name_2"));
            }


            geolocStation.setInfoNextStation(infoNextStation);

            alGeolocStations.add(geolocStation);
        }

        return alGeolocStations;
    }


    public Object[][] getRoutecolGeo(String idRoute) throws JSONException {
        WebRequest webRequest = new WebRequest();
        String data = webRequest.makeWebServiceCall(new StringBuilder(GET_ROUTECOL_GEO_URL_JSON).append(idRoute).append("/undefined/undefined").toString(), WebRequest.GETRequest);
        JSONObject jsonObject = new JSONObject(data);
        JSONArray jaFeatures = jsonObject.getJSONArray("features");
        JSONObject joFeatures = jaFeatures.getJSONObject(0);
        JSONObject joGeometry = joFeatures.getJSONObject("geometry");
        JSONArray jaGeometries = joGeometry.getJSONArray("geometries");
        JSONObject joGeometries = jaGeometries.getJSONObject(0);
        JSONArray jaCoordinates = joGeometries.getJSONArray("coordinates");
        ArrayList<Coordinates> alCoordinates = new ArrayList<>();
        ArrayList<Station> alStations = new ArrayList<>();

        for(int i=0; i<jaCoordinates.length(); i++) {

            JSONArray jaLonLat = jaCoordinates.getJSONArray(i);

            Coordinates coordinates = new Coordinates();
            coordinates.setLon(jaLonLat.getString(0));
            coordinates.setLat(jaLonLat.getString(1));
            alCoordinates.add(coordinates);

        }

        JSONArray jaStations = jsonObject.getJSONArray("stations");
        for(int i=0; i<jaStations.length(); i++) {
            JSONObject joStation = jaStations.getJSONObject(i);
            Station station = new Station();
            station.setId(joStation.getString("id"));
            station.setName(joStation.getString("name"));
            //распарсим coordinates
            station.setLon(joStation.getJSONArray("coordinates").getString(0));
            station.setLat(joStation.getJSONArray("coordinates").getString(1));
            alStations.add(station);
        }
        Object[][] resultObj = new Object[1][2];
        resultObj[0][0] = alCoordinates;
        resultObj[0][1] = alStations;

        return resultObj;
    }

    public ArrayList<Vehicle> getVehicles(String idRoute) throws JSONException {
        WebRequest webRequest = new WebRequest();
        String data = webRequest.makeWebServiceCall(new StringBuilder(GET_BUS_URL_JSON).append(idRoute).toString(), WebRequest.GETRequest);
        JSONObject jsonObject = new JSONObject(data);
        JSONArray jaVehicles = jsonObject.getJSONArray("vehicles");
        ArrayList<Vehicle> alVehicles = new ArrayList<>();

        for(int i=0; i<jaVehicles.length(); i++) {
            JSONObject joVehicle = jaVehicles.getJSONObject(i);
            Vehicle vehicle = new Vehicle();
            vehicle.setId(joVehicle.getString("id"));
            vehicle.setType(joVehicle.getString("type"));
            //распарсим coordinates
            vehicle.setLon(joVehicle.getJSONArray("coordinates").getString(0));
            vehicle.setLat(joVehicle.getJSONArray("coordinates").getString(1));
            vehicle.setInfo(joVehicle.getString("info"));
            vehicle.setCourse(joVehicle.getString("course"));

            alVehicles.add(vehicle);
        }

        return alVehicles;
    }
}
