package ru.bus55.models;

import java.io.Serializable;

/**
 * Created by IALozhnikov on 27.04.2016.
 */
public class Station implements Serializable {

    private String id;
    private String lon;
    private String lat;
    private String name;
    private String id2gis;

    private String idRoute;
    private String order;
    private String idStation;
    private String dir;

    private InfoNextStation infoNextStation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId2gis() {
        return id2gis;
    }

    public void setId2gis(String id2gis) {
        this.id2gis = id2gis;
    }

    /*Для остановок транспорта*/
    public String getIdRoute() {
        return idRoute;
    }

    public void setIdRoute(String idRoute) {
        this.idRoute = idRoute;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getIdStation() {
        return idStation;
    }

    public void setIdStation(String idStation) {
        this.idStation = idStation;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public InfoNextStation getInfoNextStation() {
        return infoNextStation;
    }

    public void setInfoNextStation(InfoNextStation infoNextStation) {
        this.infoNextStation = infoNextStation;
    }
}
