package ru.bus55.models;

import java.io.Serializable;

/**
 * Created by IALozhnikov on 25.04.2016.
 */
public class Route implements Serializable {

    private String id;
    private String id2gis;
    private String name;
    private String type;
    private String first;
    private String last;
    private String firstId;
    private String lastId;
    private String stationsString;
    private String stationsStringDir1;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId2gis() {
        return id2gis;
    }

    public void setId2gis(String id2gis) {
        this.id2gis = id2gis;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getFirstId() {
        return firstId;
    }

    public void setFirstId(String firstId) {
        this.firstId = firstId;
    }

    public String getLastId() {
        return lastId;
    }

    public void setLastId(String lastId) {
        this.lastId = lastId;
    }

    public String getStationsString() {
        return stationsString;
    }

    public void setStationsString(String stationsString) {
        this.stationsString = stationsString;
    }

    public String getStationsStringDir1() {
        return stationsStringDir1;
    }

    public void setStationsStringDir1(String stationsStringDir1) {
        this.stationsStringDir1 = stationsStringDir1;
    }
}
