package ru.bus55.models;

import java.io.Serializable;

/**
 * Created by IALozhnikov on 27.04.2016.
 */
public class Direction implements Serializable {

    private String id;
    private String idStation1;
    private String idStation2;
    private String up;
    private String routes;
    private String stationName2;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdStation1() {
        return idStation1;
    }

    public void setIdStation1(String idStation1) {
        this.idStation1 = idStation1;
    }

    public String getIdStation2() {
        return idStation2;
    }

    public void setIdStation2(String idStation2) {
        this.idStation2 = idStation2;
    }

    public String getUp() {
        return up;
    }

    public void setUp(String up) {
        this.up = up;
    }

    public String getRoutes() {
        return routes;
    }

    public void setRoutes(String routes) {
        this.routes = routes;
    }

    public String getStationName2() {
        return stationName2;
    }

    public void setStationName2(String stationName2) {
        this.stationName2 = stationName2;
    }
}
