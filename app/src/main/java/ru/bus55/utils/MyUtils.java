package ru.bus55.utils;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.List;

import ru.bus55.R;
import ru.bus55.activity.MainActivity;

public class MyUtils {
	public static float convertDpToPixel(float dp, Context context) {
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float px = dp * (metrics.densityDpi/160f);
	    return px;
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	private static Bitmap getBitmap(VectorDrawable vectorDrawable) {
		Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
				vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		vectorDrawable.draw(canvas);
		return bitmap;
	}

	private static Bitmap getBitmap(VectorDrawableCompat vectorDrawable) {
		Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
				vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		vectorDrawable.draw(canvas);
		return bitmap;
	}

	public static Bitmap getBitmap(Context context, @DrawableRes int drawableResId) {
		Drawable drawable = ContextCompat.getDrawable(context, drawableResId);
		if (drawable instanceof BitmapDrawable) {
			return ((BitmapDrawable) drawable).getBitmap();
		} else if (drawable instanceof VectorDrawableCompat) {
			return getBitmap((VectorDrawableCompat) drawable);
		} else if (drawable instanceof VectorDrawable) {
			return getBitmap((VectorDrawable) drawable);
		} else {
			throw new IllegalArgumentException("Unsupported drawable type");
		}
	}

	public static void openAppRating(Context context) {
		String packageName = "ru.bus55";

		Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
		boolean marketFound = false;

		// find all applications able to handle our rateIntent
		final List<ResolveInfo> otherApps = context.getPackageManager().queryIntentActivities(rateIntent, 0);
		for (ResolveInfo otherApp: otherApps) {
			// look for Google Play application
			if (otherApp.activityInfo.applicationInfo.packageName.equals("com.android.vending")) {

				ActivityInfo otherAppActivity = otherApp.activityInfo;
				ComponentName componentName = new ComponentName(
						otherAppActivity.applicationInfo.packageName,
						otherAppActivity.name
				);
				rateIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
				rateIntent.setComponent(componentName);
				context.startActivity(rateIntent);
				marketFound = true;
				break;

			}
		}

		// if GP not present on device, open web browser
		if (!marketFound) {
			//Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+context.getPackageName()));
			Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+packageName));
			context.startActivity(webIntent);
		}
	}

	public static void sendEmail(Context context) {
		String to = context.getResources().getString(R.string.email_error_to);
		String subject = context.getResources().getString(R.string.email_error_subject);

		Intent emailIntent = new Intent(Intent.ACTION_SENDTO);

		emailIntent.setData(Uri.parse("mailto:" + to));
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, "");

		if (emailIntent.resolveActivity(context.getPackageManager()) != null) {
			context.startActivity(emailIntent);
		} else {
			String emailClientNotFound = context.getResources().getString(R.string.email_client_not_found);
			Toast.makeText(context, emailClientNotFound, Toast.LENGTH_SHORT).show();
		}
	}


	public static Location getMyLastKnownLocation() throws SecurityException {
		LocationManager locationManager = (LocationManager) MainActivity.getContext().getSystemService(MainActivity.getContext().LOCATION_SERVICE);
		//List<String> providers = locationManager.getProviders(true);
		List<String> providers = locationManager.getAllProviders();
		Location bestLocation = null;
		Location changeLocation = null;
		for (String provider : providers) {

			Location l = locationManager.getLastKnownLocation(provider);
			if (l == null) {
				continue;
			}
			if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
				// Found best last known location: %s", l);
				bestLocation = l;
			}
		}

		return bestLocation;
	}

	public static boolean checkPlayServices(Context context) {
		GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
		int result = googleAPI.isGooglePlayServicesAvailable(context);
		if(result != ConnectionResult.SUCCESS) {
            /*
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(getActivity(), result,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            */
			return false;
		}

		return true;
	}

	/*Это конечно пиздец товарищи, но это хак века, сука, когда возвращаемся назад на карту,
        то она начинает тормозить, после того как покажем и скроем меню, тормоз карт пропадает*/
	public static void openCloseOptionMenuForHackMapViewFreezes(final Handler myHandler, final FragmentActivity activity) {

		//final Handler myHandler = new Handler();
		Thread myThread = new Thread(new Runnable() {
			@Override
			public void run() {

				myHandler.post(new Runnable() {  // используя Handler, привязанный к UI-Thread
					@Override
					public void run() {
						activity.openOptionsMenu();
						activity.closeOptionsMenu();
					}
				});
			}

		});

		myThread.start();
	}

}
