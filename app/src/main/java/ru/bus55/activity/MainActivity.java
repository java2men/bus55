package ru.bus55.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import ru.bus55.R;
import ru.bus55.adapters.MyFragmentStatePagerAdapter;
import ru.bus55.db.DatabaseHelper;
import ru.bus55.fragments.ChildTabFragment;
import ru.bus55.fragments.TabFragment;
import ru.bus55.fragments.bookmarks.BookmarksListFragment;
import ru.bus55.fragments.map.MySupportMapFragment;
import ru.bus55.fragments.routes.RouteDirectionsFragment;
import ru.bus55.fragments.routes.RouteMapFragment;
import ru.bus55.fragments.routes.RoutesListFragment;
import ru.bus55.fragments.routes.RoutesNumFilterListFragment;
import ru.bus55.fragments.settings.SettingsFragment;
import ru.bus55.fragments.stations.PredictionsStationFragment;
import ru.bus55.models.Route;
import ru.bus55.models.Station;
import ru.bus55.utils.MyUtils;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    final static int REQUEST_LOCATION = 199;

    public static int RoutesTabFragmentIndex = 0;
    public static int StationsTabFragmentIndex = 1;
    public static int BookmarksTabFragmentIndex = 2;
    public static int MapTabFragmentIndex = 3;

    public static int PERMISSION_REQUEST_CODE = 1;
    public static float DISTANCE_TO = 30.0f;

    public static int INTERVAL_LOCATION_REQUEST = 5 * 1000;
    public static int INTERVAL_FASTEST_LOCATION_REQUEST = INTERVAL_LOCATION_REQUEST * 2;

    private static Context mContext;

    private Menu menu;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MyFragmentStatePagerAdapter mfspAdapter;

    private ArrayList<Fragment> alFragments = new ArrayList<>();
    private Stack<Fragment> stackFragments = new Stack<>();
    private Fragment onBackFragment = null;

    private DatabaseHelper databaseHelper;

    //предзагрузочные List данных
    private static ArrayList<Route> alRoutes;
    private static ArrayList<Station> alStations;
    private static ArrayList<Route> alBookmarkRoutes;
    private static ArrayList<Station> alBookmarkStation;

    private boolean isExit = false;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mLastLocation;

    private boolean isMyLocationPermissions = false;

    private MySupportMapFragment mySupportMapFragment;
    private RouteMapFragment routeMapFragment;
    private boolean isOnLocationChanged = false;
    private String sourceClassBuildGoogleApiClient = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadMyLocation();
        mContext = this;
        databaseHelper = new DatabaseHelper(getContext());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final android.support.v7.app.ActionBar ab = getSupportActionBar();

        setDisplayShowHome(false);
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
        ab.setDisplayShowTitleEnabled(true); // disable the default title element here (for centered title)
        supportInvalidateOptionsMenu();


        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setCustomView(R.layout.custom_view_tab_routes), 0);
        tabLayout.addTab(tabLayout.newTab().setCustomView(R.layout.custom_view_tab_stations), 1);
        tabLayout.addTab(tabLayout.newTab().setCustomView(R.layout.custom_view_tab_bookmarks), 2);
        tabLayout.addTab(tabLayout.newTab().setCustomView(R.layout.custom_view_tab_map), 3);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        mfspAdapter = new MyFragmentStatePagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(mfspAdapter);

        //Управлять вкладками
        viewPager.setOffscreenPageLimit(tabLayout.getTabCount());
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

                //Ищем текущий фрагмент через TabFragment
                TabFragment tabFragment = ((TabFragment) mfspAdapter.getItem(viewPager.getCurrentItem()));
                ChildTabFragment currentFragment = (ChildTabFragment)tabFragment.getCurrentFragment();

                if (currentFragment != null) {
                    if (currentFragment.getActionBarTitle() != null) {
                        String abTitle = currentFragment.getActionBarTitle();
                        getSupportActionBar().setTitle(abTitle);
                    } else {
                        getSupportActionBar().setTitle(tabFragment.getTitle());
                    }

                    supportInvalidateOptionsMenu();


                    //!!!!!!!!!!
                    if (currentFragment.getClass().getName().equals(MySupportMapFragment.class.getName())) {
                        ((MySupportMapFragment)currentFragment).onResume();
                    }

                    if (currentFragment.getClass().getName().equals(RouteMapFragment.class.getName())) {
                        ((RouteMapFragment)currentFragment).onResume();
                    }

                    //проверять фрагменты вклакди маршрутов, вызывая onResume()
                    if (currentFragment.getClass().getName().equals(RoutesNumFilterListFragment.class.getName())) {
                        ((RoutesNumFilterListFragment)currentFragment).onResume();
                    }

                    if (currentFragment.getClass().getName().equals(RoutesListFragment.class.getName())) {
                        ((RoutesListFragment)currentFragment).onResume();
                    }

                } else {
                    getSupportActionBar().setTitle(tabFragment.getTitle());
                }

                mfspAdapter.notifyDataSetChanged();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                ChildTabFragment currentFragment = (ChildTabFragment) getCurrentFragment();
                if (currentFragment != null) {
                    currentFragment.hideKeyboard();
                }

                if (currentFragment.getClass().getName().equals(MySupportMapFragment.class.getName())) {
                    ((MySupportMapFragment)currentFragment).onPause();
                }

                if (currentFragment.getClass().getName().equals(RouteMapFragment.class.getName())) {
                    ((RouteMapFragment)currentFragment).onPause();
                }

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

                //При реселекте вернуть первый фрагмент
                Fragment currentFragment = getCurrentFragment();
                if (currentFragment != null) {

                    FragmentManager myFragmentManager = currentFragment.getParentFragment().getChildFragmentManager();
                    TabFragment tabFragment = (TabFragment) currentFragment.getParentFragment();
                    ChildTabFragment firstChildTabFragment = tabFragment.getFirstChildTabFragment();
                    int containerViewId = getContainerViewId(currentFragment);
                    FragmentTransaction myFragmentTransaction = myFragmentManager.beginTransaction();
                    myFragmentTransaction.replace(containerViewId, (Fragment) firstChildTabFragment);
                    myFragmentTransaction.commit();

                }

            }
        });

        //Отыкрыть закладку в соотвествии с настройками
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        int settingStartTab = Integer.valueOf(preferences.getString(SettingsFragment.PREF_KEY_LIST_TAB, "0"));

        tabLayout.getTabAt(settingStartTab).select();

        createLocationRequest();
        buildGoogleApiClient(MainActivity.class.getSimpleName());

        requestMyLocationPermissions();

    }

    private void saveMyLocation() {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Gson gson = new Gson();
        editor.putString("mLastLocation", gson.toJson(mLastLocation));
        editor.commit();

    }

    private void loadMyLocation() {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        String str = sharedPref.getString("mLastLocation", "");
        Gson gson = new Gson();
        mLastLocation = gson.fromJson(str, Location.class);
    }

    private void requestMyLocationPermissions() {


        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            isMyLocationPermissions = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    }, PERMISSION_REQUEST_CODE);


        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                mGoogleApiClient.disconnect();
            }
        }
        saveMyLocation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            Fragment currentFragment = getCurrentFragment();
            SettingsFragment settingsFragment = new SettingsFragment();

            int containerViewId = getContainerViewId(currentFragment);
            FragmentTransaction myFragmentTransaction = currentFragment.getParentFragment().getChildFragmentManager().beginTransaction();

            myFragmentTransaction.replace(containerViewId, settingsFragment);
            myFragmentTransaction.addToBackStack(null);
            myFragmentTransaction.commit();

            return true;
        }

        if (id == R.id.action_refresh) {

            Fragment currentFragment = getCurrentFragment();
            if (currentFragment instanceof ChildTabFragment) {
                ((ChildTabFragment) currentFragment).asyncTaskExecute();
            }
            return true;
        }

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        if (id == R.id.action_add_bookmarks) {
            Fragment currentFragment = getCurrentFragment();

            if (currentFragment instanceof RouteDirectionsFragment) {
                RouteDirectionsFragment routeDirectionsFragment = ((RouteDirectionsFragment) currentFragment);

                if (routeDirectionsFragment != null) {
                    routeDirectionsFragment.addRouteToBookmark();
                }
            }

            if (currentFragment instanceof PredictionsStationFragment) {
                PredictionsStationFragment predictionsStationFragment = ((PredictionsStationFragment) currentFragment);

                if (predictionsStationFragment != null) {
                    predictionsStationFragment.addStationToBookmark();
                }
            }

            //Найдем фрагмент BookmarksListFragment и обновим список
            BookmarksListFragment blf = (BookmarksListFragment) findFragment(BookmarksListFragment.class);
            blf.asyncTaskExecute();

            return true;
        }

        if (id == R.id.action_view_map) {

            Fragment currentFragment = getCurrentFragment();
            if (currentFragment instanceof RouteDirectionsFragment) {
                RouteDirectionsFragment routeDirectionsFragment = ((RouteDirectionsFragment) currentFragment);

                if (routeDirectionsFragment != null) {
                    routeDirectionsFragment.setRouteMapFragment();
                }
            }
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        OnBackPressedListener backPressedListener = null;
        Fragment onBackFragment = getCurrentFragment();


        if (onBackFragment != null) {

            if (onBackFragment instanceof OnBackPressedListener) {
                backPressedListener = (OnBackPressedListener) onBackFragment;
            }

            if (backPressedListener != null) {
                backPressedListener.onBackPressed();
            } else {
                showAlertDialogExit();
            }
        } else {
            super.onBackPressed();
        }

    }

    public interface OnBackPressedListener {
        public void onBackPressed();
    }

    public Fragment getCurrentFragment() {
        TabFragment tabFragment = (TabFragment) mfspAdapter.getItem(viewPager.getCurrentItem());
        Fragment f = ((Fragment)tabFragment).getChildFragmentManager().findFragmentById(tabFragment.getIdContent());
        return f;
    }

    public Fragment findFragment(Class<? extends Fragment> c) {
        List<Fragment> listFragments = new ArrayList<>();
        for (int i=0; i<mfspAdapter.getCount(); i++) {
            TabFragment tabFragment = ((TabFragment) mfspAdapter.getItem(i));
            listFragments.addAll(tabFragment.getListChildFragments());
        }

        for (Fragment f : listFragments) {
            if (f != null) {
                if (f.getClass().equals(c)) {
                    return f;
                }
            }
        }
        return null;
    }

    public int getContainerViewId(Fragment fragment) {

        int containerViewId = ((TabFragment)fragment.getParentFragment()).getIdContent();

        return containerViewId;
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
    }

    public boolean isShowKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        boolean active = false;
        if (imm != null && imm.isActive() &&
                imm.isAcceptingText()) {
            active = true;
        } else {
            active = false;
        }

        return active;
    }

    public void setDisplayShowHome(boolean show) {
        getSupportActionBar().setDisplayShowHomeEnabled(show);
        getSupportActionBar().setDisplayHomeAsUpEnabled(show);
    }

    public void setVisibleTabLayout(int visible) {
        tabLayout.setVisibility(visible);
    }

    public Menu getMenu() {
        return menu;
    }

    public static Context getContext(){
        return mContext;
    }

    public TabLayout getTabLayout() {
        return tabLayout;
    }

    //Получить и установить статически предзагрузочные List
    public static ArrayList<Route> getAlRoutes() {
        return alRoutes;
    }

    public static void setAlRoutes(ArrayList<Route> alRoutes) {
        MainActivity.alRoutes = alRoutes;
    }

    public static ArrayList<Station> getAlStations() {
        return alStations;
    }

    public static void setAlStations(ArrayList<Station> alStations) {
        MainActivity.alStations = alStations;
    }

    public static ArrayList<Route> getAlBookmarkRoutes() {
        return alBookmarkRoutes;
    }

    public static void setAlBookmarkRoutes(ArrayList<Route> alBookmarkRoutes) {
        MainActivity.alBookmarkRoutes = alBookmarkRoutes;
    }

    public static ArrayList<Station> getAlBookmarkStation() {
        return alBookmarkStation;
    }

    public static void setAlBookmarkStation(ArrayList<Station> alBookmarkStation) {
        MainActivity.alBookmarkStation = alBookmarkStation;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSION_REQUEST_CODE && grantResults.length == 2) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                isMyLocationPermissions = true;
            } else {
                isMyLocationPermissions = false;
                showDialogRationale();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void showDialogRationale() {
        AlertDialog.Builder ad = new AlertDialog.Builder(MainActivity.getContext());

        ad.setTitle("Невозможно установить ваше текущее местоположение");  // заголовок
        ad.setMessage("Вы запретили разрешение на определение вашего местоположения. Для того чтобы разрешить, зайдите в настройки разрешений."); // сообщение
        ad.setPositiveButton("Понятно", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                return;
            }
        });

        ad.show();
    }

    private void showAlertDialogExit() {

        AlertDialog.Builder ad = new AlertDialog.Builder(MainActivity.getContext());

        ad.setTitle("Выйти?");  // заголовок
        ad.setMessage("Вы действительно хотите это сделать?"); // сообщение
        ad.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                superOnBackPressed();
            }
        });

        ad.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                return;
            }
        });

        ad.setCancelable(false);

        ad.show();

    }

    private void superOnBackPressed(){
        super.onBackPressed();
    }


    public synchronized void buildGoogleApiClient(String sourceClass) {

        this.sourceClassBuildGoogleApiClient = sourceClass;

        createLocationRequest();

        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


    }

    private void createLocationRequest() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(INTERVAL_LOCATION_REQUEST);
        mLocationRequest.setFastestInterval(INTERVAL_FASTEST_LOCATION_REQUEST);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setNumUpdates(1);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            if (mGoogleApiClient.isConnected()) {

                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (mLastLocation != null) {
                    onLocationChanged(mLastLocation);
                } else {
                    mLastLocation = MyUtils.getMyLastKnownLocation();
                    if (mLastLocation != null) {
                        onLocationChanged(mLastLocation);
                    } else {
                        loadMyLocation();
                        enableMyLocation();
                    }
                }

                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

            }
        }

    }

    public void enableMyLocation() {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(MainActivity.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                }
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_CANCELED: {

                        if (mGoogleApiClient != null) {
                            mGoogleApiClient.disconnect();
                            mGoogleApiClient = null;
                        }

                        onLocationChanged(mLastLocation);
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {

        mLastLocation = location;

        setMyLocationForFragmentsWithMap(location);
        isOnLocationChanged = true;

    }

    public boolean setMyLocationForFragmentsWithMap(Location location) {

        if (location == null) {
            return false;
        }

        if (mySupportMapFragment == null) {
            mySupportMapFragment = (MySupportMapFragment) findFragment(MySupportMapFragment.class);
        }

        if (mySupportMapFragment != null && mySupportMapFragment.isInitGoogleMap()) {

            //установить местоположение в mySupportMapFragment при условии,
            // что нажали кнопку геолокации в mySupportMapFragment (sourceClass == MySupportMapFragment.class.getSimpleName())
            //или пока еще не было ни одного вызова OnLocationChanged(isOnLocationChanged == false )
            if ((sourceClassBuildGoogleApiClient.equals(MySupportMapFragment.class.getSimpleName())) || !isOnLocationChanged) {
                mySupportMapFragment.setLastLocation(location);
                mySupportMapFragment.setMyLocationCoordinates(location);
                sourceClassBuildGoogleApiClient = "";
            }

        }

        if (routeMapFragment == null) {
            routeMapFragment = (RouteMapFragment) findFragment(RouteMapFragment.class);
        }

        if (routeMapFragment != null && routeMapFragment.isInitGoogleMap()) {

            routeMapFragment.setLastLocation(location);
            if (routeMapFragment.isSetMyLocation()) {
                routeMapFragment.drawMyLocationCoordinates(location, true);
            } else {
                routeMapFragment.drawMyLocationCoordinates(location, false);
            }

        }

        return true;
    }

    public Location getLastLocation() {
        return mLastLocation;
    }

    public GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }

    public boolean getMyLocationAvailability() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            return LocationServices.FusedLocationApi.getLocationAvailability(mGoogleApiClient).isLocationAvailable();
        }
        return false;
    }

    public void setMySupportMapFragment(MySupportMapFragment mySupportMapFragment) {
        this.mySupportMapFragment = mySupportMapFragment;
    }

    public MySupportMapFragment getMySupportMapFragment() {
        return mySupportMapFragment;
    }

    public void setRouteMapFragment(RouteMapFragment routeMapFragment) {
        this.routeMapFragment = routeMapFragment;
    }

    public RouteMapFragment getRouteMapFragment() {
        return routeMapFragment;
    }
}
