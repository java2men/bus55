package ru.bus55.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.json.JSONException;

import ru.bus55.R;
import ru.bus55.db.DatabaseHelper;
import ru.bus55.models.JsonParser;

public class ErrorActivity extends AppCompatActivity {

    private PreloadDataAsyncTask preloadDataAsyncTask;
    private int countForIntent = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);
        Button bReloadData = (Button)findViewById(R.id.bReloadData);
        bReloadData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataPreload();
            }
        });
    }

    public class PreloadDataAsyncTask extends AsyncTask<Void, Void, Void> {

        private boolean isError = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {

                JsonParser jsonParser = new JsonParser();

                MainActivity.setAlStations(jsonParser.getStations());
                MainActivity.setAlRoutes(jsonParser.getRoutes());

                DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
                MainActivity.setAlBookmarkRoutes(databaseHelper.getRoutes(null));
                MainActivity.setAlBookmarkStation(databaseHelper.getStation(null));
            } catch (Exception e) {
                isError = true;
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (isError) {
                return;
            } else {
                Intent intent = new Intent(ErrorActivity.this, MainActivity.class);
                startActivity(intent);
            }

            finish();
        }

    }

    public void getDataPreload() {

        if(preloadDataAsyncTask !=null && preloadDataAsyncTask.getStatus() == AsyncTask.Status.RUNNING){
            return;
        }

        preloadDataAsyncTask = new PreloadDataAsyncTask();
        preloadDataAsyncTask.execute();

        new Thread(new Runnable() {
            @Override
            public void run() {
                JsonParser jsonParser = new JsonParser();

                try {
                    MainActivity.setAlStations(jsonParser.getStations());
                    synchronized (ErrorActivity.this) {
                        countForIntent++;
                    }
                } catch (JSONException e) {
                    synchronized (ErrorActivity.this) {
                        countForIntent += 2;
                    }
                    e.printStackTrace();
                }

            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                JsonParser jsonParser = new JsonParser();

                try {
                    MainActivity.setAlRoutes(jsonParser.getRoutes());
                    synchronized (ErrorActivity.this) {
                        countForIntent++;
                    }
                } catch (JSONException e) {
                    synchronized (ErrorActivity.this) {
                        countForIntent += 2;
                    }
                    e.printStackTrace();
                }

            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {

                DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
                MainActivity.setAlBookmarkRoutes(databaseHelper.getRoutes(null));
                MainActivity.setAlBookmarkStation(databaseHelper.getStation(null));
                synchronized (ErrorActivity.this) {
                    countForIntent++;
                }

            }
        }).start();
    }

}
