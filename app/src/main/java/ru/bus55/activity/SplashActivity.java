package ru.bus55.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import org.json.JSONException;

import ru.bus55.R;
import ru.bus55.db.DatabaseHelper;
import ru.bus55.fragments.map.MySupportMapFragment;
import ru.bus55.models.JsonParser;

public class SplashActivity extends AppCompatActivity {

    private int countForIntent = 0;
    private static MySupportMapFragment mySupportMapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getDataPreload();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public class PreloadDataAsyncTask extends AsyncTask<Void, Void, Void> {

        private boolean isError = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {

            while(countForIntent < 3) {

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (countForIntent > 3) {
                Intent intent = new Intent(SplashActivity.this, ErrorActivity.class);
                startActivity(intent);
            }

            if (countForIntent == 3) {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
            }

            finish();
        }

    }

    public void getDataPreload() {

        PreloadDataAsyncTask preloadDataAsyncTask = new PreloadDataAsyncTask();
        preloadDataAsyncTask.execute();

        new Thread(new Runnable() {
            @Override
            public void run() {
                JsonParser jsonParser = new JsonParser();

                try {
                    MainActivity.setAlStations(jsonParser.getStations());
                    synchronized (SplashActivity.this) {
                        countForIntent++;
                    }
                } catch (JSONException e) {
                    synchronized (SplashActivity.this) {
                        countForIntent += 2;
                    }
                    e.printStackTrace();
                }

            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                JsonParser jsonParser = new JsonParser();

                try {
                    MainActivity.setAlRoutes(jsonParser.getRoutes());
                    synchronized (SplashActivity.this) {
                        countForIntent++;
                    }
                } catch (JSONException e) {
                    synchronized (SplashActivity.this) {
                        countForIntent += 2;
                    }
                    e.printStackTrace();
                }

            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {

                DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
                MainActivity.setAlBookmarkRoutes(databaseHelper.getRoutes(null));
                MainActivity.setAlBookmarkStation(databaseHelper.getStation(null));
                synchronized (SplashActivity.this) {
                    countForIntent++;
                }

            }
        }).start();
    }

    public static MySupportMapFragment getMySupportMapFragment() {
        return mySupportMapFragment;
    }

}
