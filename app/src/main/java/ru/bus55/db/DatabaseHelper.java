package ru.bus55.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import ru.bus55.models.Direction;
import ru.bus55.models.InfoNextStation;
import ru.bus55.models.Route;
import ru.bus55.models.Station;
import ru.bus55.models.StationWithDirection;

/**
 * Created by IALozhnikov on 10.06.2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private Context myContext;
    public SQLiteDatabase myDataBase;
    private static final String DATABASE_NAME = "bookmarksstore.db"; // название бд
    private static final int SCHEMA = 1; // версия базы данных
    static final String TABLE_BOOKMARK_ROUTES = "bookmark_routes"; // название таблицы в бд
    static final String TABLE_BOOKMARK_STATIONS = "bookmark_stations"; // название таблицы в бд
    static final String TABLE_BOOKMARK_DIRECTIONS = "bookmark_directions"; // название таблицы в бд
    static final String TABLE_BOOKMARK_STATION_WITH_DIRECTION = "bookmark_station_with_direction"; // название таблицы в бд

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, SCHEMA);
        this.myContext = context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_BOOKMARK_ROUTES + " ("
                + " _id integer primary key autoincrement,"
                + " id text, "
                + " id_2gis text, "
                + " name text, "
                + " type text, "
                + " first text, "
                + " last text, "
                + " first_id text, "
                + " last_id text, "
                + " stations_string text, "
                + " stations_string_dir_1 text "
                + ");");

        db.execSQL("create table " + TABLE_BOOKMARK_STATIONS + " ("
                + " _id integer primary key autoincrement,"
                + " id_station text, "
                + " lon text, "
                + " lat text, "
                + " name text, "
                + " id_2gis text, "
                + " id_station_1 text, "
                + " id_station_2 text, "
                + " station_name_2 text "
                + ");");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_BOOKMARK_ROUTES);
        db.execSQL("drop table if exists " + TABLE_BOOKMARK_STATIONS);
        db.execSQL("drop table if exists " + TABLE_BOOKMARK_DIRECTIONS);
        onCreate(db);
    }

    public boolean insertRoute (Route route) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", route.getId());
        contentValues.put("id_2gis", route.getId2gis());
        contentValues.put("name", route.getName());
        contentValues.put("type", route.getType());
        contentValues.put("first", route.getFirst());
        contentValues.put("last", route.getLast());
        contentValues.put("first_id", route.getFirstId());
        contentValues.put("last_id", route.getLastId());
        contentValues.put("stations_string", route.getStationsString());
        contentValues.put("stations_string_dir_1", route.getStationsStringDir1());
        db.insert(TABLE_BOOKMARK_ROUTES, null, contentValues);
        db.close();
        return true;
    }

    public boolean removeRoute (Route route) {
        SQLiteDatabase db = this.getWritableDatabase();
        int delCount = db.delete(TABLE_BOOKMARK_ROUTES, "id = " + route.getId(), null);
        db.close();
        return true;
    }

    public ArrayList<Route> getRoutes1(String id) {
        ArrayList<Route> alRoutes = new ArrayList<Route>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select * from " + TABLE_BOOKMARK_ROUTES + " where id=" + id, null);
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false) {
            Route route = new Route();
            route.setId(cursor.getString(cursor.getColumnIndex("id")));
            route.setId2gis(cursor.getString(cursor.getColumnIndex("id_2gis")));
            route.setName(cursor.getString(cursor.getColumnIndex("name")));
            route.setType(cursor.getString(cursor.getColumnIndex("type")));
            route.setFirst(cursor.getString(cursor.getColumnIndex("first")));
            route.setLast(cursor.getString(cursor.getColumnIndex("last")));
            route.setFirstId(cursor.getString(cursor.getColumnIndex("first_id")));
            route.setLastId(cursor.getString(cursor.getColumnIndex("last_id")));
            route.setStationsString(cursor.getString(cursor.getColumnIndex("stations_string")));
            route.setStationsStringDir1(cursor.getString(cursor.getColumnIndex("stations_string_dir_1")));

            alRoutes.add(route);
            cursor.moveToNext();
        }
        return alRoutes;
    }

    public ArrayList<Route> getRoutes() {
        ArrayList<Route> alRoutes = new ArrayList<Route>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select * from " + TABLE_BOOKMARK_ROUTES, null);
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false) {
            Route route = new Route();
            route.setId(cursor.getString(cursor.getColumnIndex("id")));
            route.setId2gis(cursor.getString(cursor.getColumnIndex("id_2gis")));
            route.setName(cursor.getString(cursor.getColumnIndex("name")));
            route.setType(cursor.getString(cursor.getColumnIndex("type")));
            route.setFirst(cursor.getString(cursor.getColumnIndex("first")));
            route.setLast(cursor.getString(cursor.getColumnIndex("last")));
            route.setFirstId(cursor.getString(cursor.getColumnIndex("first_id")));
            route.setLastId(cursor.getString(cursor.getColumnIndex("last_id")));
            route.setStationsString(cursor.getString(cursor.getColumnIndex("stations_string")));
            route.setStationsStringDir1(cursor.getString(cursor.getColumnIndex("stations_string_dir_1")));

            alRoutes.add(route);
            cursor.moveToNext();
        }
        return alRoutes;
    }

    public ArrayList<Route> getRoutes(Route r) {
        ArrayList<Route> alRoutes = new ArrayList<Route>();
            SQLiteDatabase db = this.getReadableDatabase();

            String query = "";
            if (r == null) {
                query = "select * from " + TABLE_BOOKMARK_ROUTES;
            } else {
                query = "select * from " + TABLE_BOOKMARK_ROUTES + " where id=" + r.getId();
            }

            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();

            while (cursor.isAfterLast() == false) {
                Route route = new Route();
                route.setId(cursor.getString(cursor.getColumnIndex("id")));
                route.setId2gis(cursor.getString(cursor.getColumnIndex("id_2gis")));
                route.setName(cursor.getString(cursor.getColumnIndex("name")));
                route.setType(cursor.getString(cursor.getColumnIndex("type")));
                route.setFirst(cursor.getString(cursor.getColumnIndex("first")));
                route.setLast(cursor.getString(cursor.getColumnIndex("last")));
                route.setFirstId(cursor.getString(cursor.getColumnIndex("first_id")));
                route.setLastId(cursor.getString(cursor.getColumnIndex("last_id")));
                route.setStationsString(cursor.getString(cursor.getColumnIndex("stations_string")));
                route.setStationsStringDir1(cursor.getString(cursor.getColumnIndex("stations_string_dir_1")));

                alRoutes.add(route);
                cursor.moveToNext();
            }

            return alRoutes;
    }

    public boolean insertStationWithDirection1 (StationWithDirection stationWithDirection) {
        SQLiteDatabase db = this.getWritableDatabase();

        Direction direction = stationWithDirection.getDirection();
        Station station = stationWithDirection.getStation();

        ContentValues contentValues = new ContentValues();

        contentValues.put("id_direction", direction.getId());
        contentValues.put("id_station_1", direction.getIdStation1());
        contentValues.put("id_station_2", direction.getIdStation2());
        contentValues.put("up", direction.getUp());
        contentValues.put("routes", direction.getRoutes());
        contentValues.put("station_name_2", direction.getStationName2());

        long _idDirections = db.insert(TABLE_BOOKMARK_DIRECTIONS, null, contentValues);

        contentValues = new ContentValues();

        contentValues.put("id_station", station.getId());
        contentValues.put("lon", station.getLon());
        contentValues.put("lat", station.getLat());
        contentValues.put("name", station.getName());
        contentValues.put("id_2gis", station.getId2gis());
        contentValues.put("_id_direction", _idDirections);

        db.insert(TABLE_BOOKMARK_STATIONS, null, contentValues);

        db.close();
        return true;
    }

    public boolean removeStationWithDirection1(StationWithDirection stationWithDirection) {
        SQLiteDatabase db = this.getWritableDatabase();
        int delCount = db.delete(TABLE_BOOKMARK_STATIONS, "id_station = " + stationWithDirection.getStation().getId(), null);
        delCount = db.delete(TABLE_BOOKMARK_DIRECTIONS, "id_direction = " + stationWithDirection.getDirection().getId(), null);
        db.close();
        return true;
    }

    public ArrayList<StationWithDirection> getStationWithDirection1(StationWithDirection fswd) {
        ArrayList<StationWithDirection> alStationWithDirection = new ArrayList<StationWithDirection>();

        SQLiteDatabase db = this.getReadableDatabase();
        String query;
        if (fswd == null) {
            query = "select * from " + TABLE_BOOKMARK_STATIONS + " inner join " + TABLE_BOOKMARK_DIRECTIONS + " on " +
                    TABLE_BOOKMARK_STATIONS + "._id_direction = " + TABLE_BOOKMARK_DIRECTIONS + "._id";
        } else {
            query = "select * from " + TABLE_BOOKMARK_STATIONS + " inner join " + TABLE_BOOKMARK_DIRECTIONS + " on " +
                    TABLE_BOOKMARK_STATIONS + "._id_direction = " + TABLE_BOOKMARK_DIRECTIONS + "._id where " +
                    TABLE_BOOKMARK_STATIONS + ".id_station = " + fswd.getStation().getId() +
                    " and " + TABLE_BOOKMARK_DIRECTIONS + ".id_direction = " + fswd.getDirection().getId();
        }
        Cursor cursor =  db.rawQuery(query, null);
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false) {
            Station station = new Station();
            station.setIdStation(cursor.getString(cursor.getColumnIndex("id_station")));
            station.setName(cursor.getString(cursor.getColumnIndex("name")));
            station.setId2gis(cursor.getString(cursor.getColumnIndex("id_2gis")));

            Direction direction = new Direction();
            direction.setId(cursor.getString(cursor.getColumnIndex("id_direction")));
            direction.setIdStation1(cursor.getString(cursor.getColumnIndex("id_station_1")));
            direction.setIdStation2(cursor.getString(cursor.getColumnIndex("id_station_2")));
            direction.setUp(cursor.getString(cursor.getColumnIndex("up")));
            direction.setRoutes(cursor.getString(cursor.getColumnIndex("routes")));
            direction.setStationName2(cursor.getString(cursor.getColumnIndex("station_name_2")));

            StationWithDirection stationWithDirection = new StationWithDirection();
            stationWithDirection.setStation(station);
            stationWithDirection.setDirection(direction);

            alStationWithDirection.add(stationWithDirection);
            cursor.moveToNext();
        }

        return alStationWithDirection;
    }

    public ArrayList<StationWithDirection> getStationWithDirection1(String idStation, String idDirection) {
        ArrayList<StationWithDirection> alStationWithDirection = new ArrayList<StationWithDirection>();

        SQLiteDatabase db = this.getReadableDatabase();
        String query;

        query = "select * from " + TABLE_BOOKMARK_STATIONS + " inner join " + TABLE_BOOKMARK_DIRECTIONS + " on " +
                TABLE_BOOKMARK_STATIONS + "._id_direction = " + TABLE_BOOKMARK_DIRECTIONS + "._id where " +
                TABLE_BOOKMARK_STATIONS + ".id_station = " + idStation +
                " and " + TABLE_BOOKMARK_DIRECTIONS + ".id_direction = " + idDirection;

        Cursor cursor =  db.rawQuery(query, null);
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false) {
            Station station = new Station();
            station.setIdStation(cursor.getString(cursor.getColumnIndex("id_station")));
            station.setName(cursor.getString(cursor.getColumnIndex("name")));
            station.setId2gis(cursor.getString(cursor.getColumnIndex("id_2gis")));

            Direction direction = new Direction();
            direction.setId(cursor.getString(cursor.getColumnIndex("id_direction")));
            direction.setIdStation1(cursor.getString(cursor.getColumnIndex("id_station_1")));
            direction.setIdStation2(cursor.getString(cursor.getColumnIndex("id_station_2")));
            direction.setUp(cursor.getString(cursor.getColumnIndex("up")));
            direction.setRoutes(cursor.getString(cursor.getColumnIndex("routes")));
            direction.setStationName2(cursor.getString(cursor.getColumnIndex("station_name_2")));

            StationWithDirection stationWithDirection = new StationWithDirection();
            stationWithDirection.setStation(station);
            stationWithDirection.setDirection(direction);

            alStationWithDirection.add(stationWithDirection);
            cursor.moveToNext();
        }

        return alStationWithDirection;
    }

    public boolean insertStation (Station station) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues = new ContentValues();

        contentValues.put("id_station", station.getId());
        contentValues.put("lon", station.getLon());
        contentValues.put("lat", station.getLat());
        contentValues.put("name", station.getName());
        contentValues.put("id_2gis", station.getId2gis());
        contentValues.put("id_station_1", station.getInfoNextStation().getIdStation1());
        contentValues.put("id_station_2", station.getInfoNextStation().getIdStation2());
        contentValues.put("station_name_2", station.getInfoNextStation().getStationName2());

        db.insert(TABLE_BOOKMARK_STATIONS, null, contentValues);

        db.close();
        return true;
    }

    public boolean removeStation(Station station) {
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = "name=? and id_station_1=? and id_station_2=? and station_name_2=?";
        String[] whereArgs = new String[]{station.getName(), station.getInfoNextStation().getIdStation1(), station.getInfoNextStation().getIdStation2(), station.getInfoNextStation().getStationName2()};
        int delCount = db.delete(TABLE_BOOKMARK_STATIONS, whereClause, whereArgs);
        db.close();
        if (delCount > 0) return true;
        return false;
    }

    public ArrayList<Station> getStation(Station fStation) {
        ArrayList<Station> alStation = new ArrayList<Station>();

        SQLiteDatabase db = this.getReadableDatabase();
        String query;
        if (fStation == null) {
            query = "select * from " + TABLE_BOOKMARK_STATIONS;
        } else {
            /*для того чтобы идентифицировать одинаковые остановки, должны совпадать 4 условия
            name, id_station1, id_station2, station_name_2*/
            query = "select * from " + TABLE_BOOKMARK_STATIONS + " where " +
                    //TABLE_BOOKMARK_STATIONS + ".id_station = " + fStation.getIdStation() + " and "+
                    TABLE_BOOKMARK_STATIONS + ".name = '" + fStation.getName() + "' and "+
                    TABLE_BOOKMARK_STATIONS + ".id_station_1 = '" + fStation.getInfoNextStation().getIdStation1() + "' and "+
                    TABLE_BOOKMARK_STATIONS + ".id_station_2 = '" + fStation.getInfoNextStation().getIdStation2() + "' and "+
                    TABLE_BOOKMARK_STATIONS + ".station_name_2 = '" + fStation.getInfoNextStation().getStationName2() + "'";
        }
        Cursor cursor =  db.rawQuery(query, null);
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false) {
            Station station = new Station();
            station.setIdStation(cursor.getString(cursor.getColumnIndex("id_station")));
            station.setName(cursor.getString(cursor.getColumnIndex("name")));
            station.setId2gis(cursor.getString(cursor.getColumnIndex("id_2gis")));
            station.setInfoNextStation(new InfoNextStation(
                    cursor.getString(cursor.getColumnIndex("id_station_1")),
                    cursor.getString(cursor.getColumnIndex("id_station_2")),
                    cursor.getString(cursor.getColumnIndex("station_name_2"))));

            alStation.add(station);
            cursor.moveToNext();
        }

        return alStation;
    }


    public String getRoutesDbMode(String routesJSON){
        String routesDB = routesJSON.replaceAll(",", "-");
        return routesDB;
    }

    public String getRoutesJSONMode(String routesDB){
        String routesJSON = routesDB.replaceAll("-", ",");
        return routesJSON;
    }

}
